<?php
return [
  'backend' => [
    'frontName' => 'admin'
  ],
  'install' => [
    'date' => 'Tue, 26 Jun 2018 11:34:28 +0000'
  ],
  'crypt' => [
    'key' => 'E8JAyTAXQh4BdYLM3NJafsBCgzj0Ahjd'
  ],
  'session' => [
    'save' => 'files'
  ],
  'db' => [
    'table_prefix' => '',
    'connection' => [
      'default' => [
        'host' => 'localhost',
        'dbname' => 'frgegkpacq',
        'username' => 'frgegkpacq',
        'password' => '7DJ6BauZYM',
        'model' => 'mysql4',
        'engine' => 'innodb',
        'initStatements' => 'SET NAMES utf8;',
        'active' => '1'
      ]
    ]
  ],
  'resource' => [
    'default_setup' => [
      'connection' => 'default'
    ]
  ],
  'x-frame-options' => 'SAMEORIGIN',
  'MAGE_MODE' => 'developer',
  'cache_types' => [
    'config' => 1,
    'layout' => 1,
    'block_html' => 1,
    'collections' => 1,
    'reflection' => 1,
    'db_ddl' => 1,
    'eav' => 1,
    'config_integration' => 1,
    'config_integration_api' => 1,
    'full_page' => 0,
    'translate' => 1,
    'config_webservice' => 1,
    'customer_notification' => 1
  ]
];
