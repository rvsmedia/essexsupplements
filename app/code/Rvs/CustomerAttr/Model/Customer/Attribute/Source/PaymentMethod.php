<?php
/**
 * A Magento 2 module named Rvs/CustomerAttr
 * Copyright (C) 2018  
 * 
 * This file is part of Rvs/CustomerAttr.
 * 
 * Rvs/CustomerAttr is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

namespace Rvs\CustomerAttr\Model\Customer\Attribute\Source;

class PaymentMethod extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource
{

    /**
     * getAllOptions
     *
     * @return array
     */
    public function getAllOptions()
    {
        if ($this->_options === null) {
            $this->_options = [
                ['value' => (string) 'Paypal', 'label' => __('Paypal')],
                ['value' => (string) 'Credit Elite', 'label' => __('Credit Elite')],
                ['value' => (string) 'Credit factoring', 'label' => __('Credit factoring')]
            ];
        }
        return $this->_options;
    }
}
