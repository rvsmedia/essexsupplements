var config = {
    map: {
        '*': {
      		'Magento_Checkout/template/summary/item/details.html': 
          	'Rvs_General/template/summary/item/details.html',
          	'Magento_Checkout/js/view/summary/item/details':'Rvs_General/js/view/summary/item/details'
        }
  	},
  	config: {
        mixins: {
            'Magento_Swatches/js/swatch-renderer': {
                'Rvs_General/js/swatch-renderer-mixin': true
            }
        }
    }
};