<?php

namespace Rvs\General\Block\Catalog;

class Category extends \Magento\Framework\View\Element\Template
{
	protected $_registry;
	protected $_stockItemRepository;
	protected $_categoryFactory;
    protected $httpContext;

	public function __construct(
		\Magento\Backend\Block\Template\Context $context,        
        \Magento\CatalogInventory\Model\Stock\StockItemRepository $stockItemRepository,
		\Magento\Framework\Registry $registry,
		\Magento\Catalog\Model\CategoryFactory $categoryFactory,
		\Magento\Customer\Model\Session $customerSession,
		array $data = []
	)
	{
		$this->_registry = $registry;
		$this->_categoryFactory = $categoryFactory;
		$this->_stockItemRepository = $stockItemRepository;
		$this->_customerSession = $customerSession;
		parent::__construct($context, $data);
	}

	public function getCurrentCategory()
	{
		$categories = $this->_registry->registry('current_category')->getChildren();
		if($categories)
		{
			$categoryArray = [];
			foreach(explode(',',$categories) as $category)
			{
				$catObj = $this->_categoryFactory->create()->load($category);
				$categoryArray[] = ['link' => $catObj->getUrl(), 'title' => $catObj->getName(), 'product_count' => count($catObj->getProductCollection())];
			}
			return $categoryArray;
		}
	}
	
	public function getCurrentProduct()
    {        
        return $this->_registry->registry('current_product');
    }   
	
	 public function getStockItem($productId)
    {
        return $this->_stockItemRepository->get($productId);
    }
	
	public function getCustomer()
{
    $id = $this->_customerSession->getCustomer()->getId(); //Print current customer ID
    $customerData = $this->_customerSession->getCustomer(); 
		return $customerData;

    // print_r($customerData->getData());
}
}