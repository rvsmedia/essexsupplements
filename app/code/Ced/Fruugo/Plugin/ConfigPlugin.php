<?php
namespace Ced\Fruugo\Plugin;


class ConfigPlugin
{

    /**
     * DataHelper
     * @var \Ced\Fruugo\Helper\Data
     */
//    public $dataHelper;

    public function afterSave(
        \Magento\Config\Model\Config $subject
//        \Closure $proceed
    ) {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $scopeConfigManager = $objectManager->create('Magento\Framework\App\Config\ScopeConfigInterface');

        $configPost = $subject->getData();
        /* echo '<pre>';
         print_r($configPost);die;*/
        if(isset($configPost['section']) && $configPost['section'] == 'fruugoconfiguration'/* && isset($configPost['groups']['fruugosetting']['fields']['orders_fetch_startdate']['value'])*/) {
            /*$url = $scopeConfigManager->getValue('fruugoconfiguration/fruugosetting/api_url');
            //$url = $configPost['groups']['fruugosetting']['fields']['api_url']['value'];
            $url = $url.'v3/orders?createdStartDate='.$configPost['groups']['fruugosetting']['fields']['orders_fetch_startdate']['value'];
            $privateKey = $configPost['groups']['fruugosetting']['fields']['private_key']['value'];
            $channelId= \Ced\Fruugo\Helper\Data::CONSUMER_CHANEL_TYPE_ID;
            $consumerId = $configPost['groups']['fruugosetting']['fields']['customer_id']['value'];
            $signatureHelper = $objectManager->create('\Ced\Fruugo\Helper\Signature');*/
            $this->dataHelper = $objectManager->create('\Ced\Fruugo\Helper\Data');
            $configResourceModel = $objectManager->create('\Magento\Config\Model\ResourceModel\Config');
            /*$signature = $signatureHelper->getSignature($url, 'GET', null, $consumerId, $privateKey);
            $timestamp = $signatureHelper->timestamp;

            $params= [
                'consumer_id'=> $consumerId,
                'url' =>  $url,
                'signature' => $signature,
                'timestamp' => $timestamp,
                'headers' => 'WM_CONSUMER.CHANNEL.TYPE: '. $channelId,
            ];*/

            $response = $this->dataHelper->validateCredentials('stockstatus-api');
            //echo "<pre>";print_r($response);die('fg');
            //if ($this->dataHelper->debugMode) {
            //    $this->dataHelper->fruugoLogger
            //        ->debug("Fruugo->Controller->Config->Validate->Validate.php->execute() :  \n Request :".
            //           " \n URL: ". $url.
            //          "\n Body : \n". var_export($params, true) .
            //           "\n Response : \n " . var_export($response, true)
            //       );
            //}

            $messageManager = $objectManager->create('\Magento\Framework\Message\ManagerInterface');

            $enabled = $scopeConfigManager->getValue('fruugoconfiguration/fruugosetting/enable');

            if ($enabled && $response) {
                $messageManager->addSuccess('Fruugo Credential Valid');
                $configResourceModel->saveConfig('fruugoconfiguration/fruugosetting/validate_details','1','default',0);
            } else {
                $messageManager->addError('Fruugo Credential Invalid');
                $configResourceModel->saveConfig('fruugoconfiguration/fruugosetting/validate_details','0','default',0);
            }

        }
//        return $proceed();
    }
}