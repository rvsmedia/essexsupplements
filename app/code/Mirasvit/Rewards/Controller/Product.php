<?php
/**
 * Mirasvit
 *
 * This source file is subject to the Mirasvit Software License, which is available at https://mirasvit.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Mirasvit
 * @package   mirasvit/module-rewards
 * @version   2.3.21
 * @copyright Copyright (C) 2019 Mirasvit (https://mirasvit.com/)
 */



namespace Mirasvit\Rewards\Controller;

use Magento\Framework\App\Action\Action;

/**
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
abstract class Product extends Action
{
    protected $customerSession;
    protected $earnHelper;
    protected $productRepository;
    protected $rewardsDataHelper;

    public function __construct(
        \Mirasvit\Rewards\Helper\Data                   $rewardsDataHelper,
        \Mirasvit\Rewards\Helper\Balance\Earn           $earnHelper,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Magento\Customer\Model\Session                 $customerSession,
        \Magento\Framework\App\Action\Context           $context
    ) {
        $this->rewardsDataHelper = $rewardsDataHelper;
        $this->earnHelper        = $earnHelper;
        $this->productRepository = $productRepository;
        $this->customerSession   = $customerSession;

        parent::__construct($context);
    }

    /************************/
}
