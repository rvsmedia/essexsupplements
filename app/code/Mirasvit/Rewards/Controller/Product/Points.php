<?php
/**
 * Mirasvit
 *
 * This source file is subject to the Mirasvit Software License, which is available at https://mirasvit.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Mirasvit
 * @package   mirasvit/module-rewards
 * @version   2.3.21
 * @copyright Copyright (C) 2019 Mirasvit (https://mirasvit.com/)
 */



namespace Mirasvit\Rewards\Controller\Product;

use Magento\Framework\Controller\ResultFactory;

class Points extends \Mirasvit\Rewards\Controller\Product
{
    /**
     * @return void
     */
    public function execute()
    {
        $data = $this->getRequest()->getParams();

        $product         = $this->productRepository->getById((int)$data['product_id']);
        $productPrice    = (float)$data['price'] * (int)$data['qty'];
        $customerGroupId = $this->customerSession->getCustomerGroupId();

        $points = $this->earnHelper->getProductPoints($product, $customerGroupId, 0, $productPrice);
        $label  = __('Earn %1', $this->rewardsDataHelper->formatPoints($points));

        $response = [
            'points' => $points,
            'label'  => $label,
        ];
        if ($this->getRequest()->isXmlHttpRequest()) {
            echo json_encode($response);
            exit;
        }
    }
}
