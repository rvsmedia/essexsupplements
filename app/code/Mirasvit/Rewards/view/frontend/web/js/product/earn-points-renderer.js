define([
    'jquery',
    'priceUtils',
    'priceBox'
], function ($) {
    var activeRequests = 0;
    $.widget('mage.earnPointsRenderer', {
        options: {},
        pointsXhr: [],
        priceBoxEl: null,
        productId: null,
        productMainBlock: null,
        isCategoryPage: false,

        _init: function() {
            var self = this;

            this.initData();

            $('.price-box', this.productMainBlock).on('updatePrice', function(e, data) {
                if (!data || !$('.rewards__product-earn-points', this).length || $('.page-product-bundle').length) {
                    return;
                }

                var option = {};
                if (typeof data.prices == 'undefined') { // configurable
                    for (var i in data) {
                        option   = data[i];
                        break;
                    }
                } else { //swatches
                    option = data.prices;
                }
                if (typeof option.rewardsBasePrice == 'undefined') {
                    return;
                }
                var currentPrice = self.element.attr('data-rewards-base-price');

                var originPrice = self.element.attr('data-origin-rewards-base-price')*1;
                var price = option.rewardsBasePrice.amount + originPrice;
                if (price != currentPrice) {
                    self.element.attr('data-rewards-base-price', price);
                    self.requestPoints();
                }

                return;
            });

            //bundle
            $('#product_addtocart_form').on('updateProductSummary', function(e, data) {
                if ($(self.element).closest('.fieldset-bundle-options').length) {
                    var price = self.element.attr('data-rewards-base-price');
                    self.element.attr('data-rewards-base-price', price);
                    self.requestPoints(1);
                    return;
                }
                var price = 0;
                if (data.config.selected.length) {
                    $.each(data.config.selected, function (index, values) {
                        $.each(values, function (i, value) {
                            if (!value) {
                                return;
                            }
                            var qty = data.config.options[index]['selections'][value]['qty'];
                            price += data.config.options[index]['selections'][value]['rewardsBasePrice']['amount'] * qty;
                        })
                    });
                } else {
                    if (self.element.attr('data-rewards-min-price') > 0 && self.element.closest('.price-from').length) {
                        price = self.element.attr('data-rewards-min-price');
                    }
                    if (self.element.attr('data-rewards-max-price') > 0 && self.element.closest('.price-to').length) {
                        price = self.element.attr('data-rewards-max-price');
                    }
                }
                var currentPrice = self.element.attr('data-rewards-base-price');

                if (currentPrice != price) {
                    self.element.attr('data-rewards-base-price', price);
                    self.requestPoints(1);
                }
            });

            //
            $('.input-text.qty', self.productMainBlock).keyup(function() {
                if ($('.page-product-bundle').length) {
                    return;
                }
                var qty = $(this).val();
                var parent = $(this).parents('tr');
                if (!parent.length) {
                    parent = $(this).parents('.product-info-main');
                }
                if (!parent.length) {
                    return;
                }

                self.requestPoints(qty);
            });

            if (!$('.page-product-bundle').length) {
                this.requestPoints()
            }
        },
        initData: function() {
            this.priceBoxEl       = $(this.element).closest('.price-box');
            this.productId        = $(this.priceBoxEl).attr('data-product-id');
            this.productMainBlock = $(this.element).closest('tr');
            if (!this.productId) {
                this.productId = $(this.element).attr('data-product-id');
            }
            if (!this.productMainBlock.length) {
                this.productMainBlock = $(this.element).closest('.product-info-main');
            }
            // bundle summary
            if (!this.productMainBlock.length) {
                this.productMainBlock = $(this.element).closest('.product-details');
            }
            // category page
            if (!this.productMainBlock.length) {
                this.isCategoryPage = true;
                this.productMainBlock = $(this.element).closest('.product-item-info');
            }
        },

        requestPoints: function(newQty) {
            var self      = this;
            var qty       = 0;
            var isUpdated = $('.price', this.element).attr('data-points-updated');
            var price     = parseFloat(this.element.attr('data-rewards-base-price')) || 0;

            if (!this.productMainBlock.length && isUpdated) {
                return;
            }
            if (!this.productMainBlock.length) {
                qty = 1;
            } else {
                qty = $('#qty', this.productMainBlock).val();
            }
            if (!qty) {
                qty = newQty;
            }
            if (this.isCategoryPage) {
                if (!qty) {
                    qty = 1;
                }

                if (price <= 0) {
                    if (self.element.attr('data-rewards-min-price') > 0 && self.element.closest('.price-from').length) {
                        price = self.element.attr('data-rewards-min-price');
                    }
                    if (self.element.attr('data-rewards-max-price') > 0 && self.element.closest('.price-to').length) {
                        price = self.element.attr('data-rewards-max-price');
                    }
                }
            }
            if (!qty || !this.productId) {
                $('.price', self.element).html('');
                return;
            }
            if ((this.element.closest('.price-to').length || this.element.closest('.price-from').length) && isUpdated) {
                return;
            }
            if (activeRequests >= 6) {
                setTimeout(function() {
                    self.requestPoints()
                }, 500);
                return;
            }
            $('.price', self.element).attr('data-points-updated', 1).show();

            $('.points-loader', self.element).show();
            activeRequests++;
            this.pointsXhr[this.productId] = $.ajax({
                url: this.options.requestUrl,
                cache: true,
                type: 'GET',
                dataType: 'json',
                data: {
                    'product_id': this.productId,
                    'price': price,
                    'qty': qty,
                    'isAjax': true,
                },
                success: function (data) {
                    if (data.points > 0) {
                        $('.price', self.element).html(data.label);
                    } else {
                        $('.price', self.element).html('');
                    }
                    $('.points-loader', self.element).hide();
                },
                complete: function() {
                    activeRequests--;
                }
            });
        }
    });


    return $.mage.earnPointsRenderer;
});