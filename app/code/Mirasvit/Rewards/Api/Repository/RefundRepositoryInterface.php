<?php
/**
 * Mirasvit
 *
 * This source file is subject to the Mirasvit Software License, which is available at https://mirasvit.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Mirasvit
 * @package   mirasvit/module-rewards
 * @version   2.3.21
 * @copyright Copyright (C) 2019 Mirasvit (https://mirasvit.com/)
 */



namespace Mirasvit\Rewards\Api\Repository;

use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\InputException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\StateException;
use Mirasvit\Rewards\Api\Data\RefundInterface;

interface RefundRepositoryInterface
{
    /**
     * @param RefundInterface $refund
     * @return RefundInterface
     * @throws \Exception
     */
    public function save(RefundInterface $refund);

    /**
     * @param int $refundId
     * @return RefundInterface
     */
    public function get($refundId);

    /**
     * @param int $creditmemoId
     * @return RefundInterface
     */
    public function getByCreditmemoId($creditmemoId);

    /**
     * @param RefundInterface $refund
     * @return bool Will returned True if deleted
     * @throws \Exception
     */
    public function delete(RefundInterface $refund);
}