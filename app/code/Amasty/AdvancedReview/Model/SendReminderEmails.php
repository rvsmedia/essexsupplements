<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2019 Amasty (https://www.amasty.com)
 * @package Amasty_AdvancedReview
 */


namespace Amasty\AdvancedReview\Model;

use Amasty\AdvancedReview\Model\ResourceModel\Reminder\ReadyToSendCollectionFactory;
use Amasty\AdvancedReview\Model\Repository\ReminderRepository;
use Psr\Log\LoggerInterface;

class SendReminderEmails
{
    /**
     * @var ReminderRepository
     */
    private $reminderRepository;

    /**
     * @var ReadyToSendCollectionFactory
     */
    private $collectionFactory;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var array
     */
    private $data;

    public function __construct(
        ReminderRepository $reminderRepository,
        ReadyToSendCollectionFactory $collectionFactory,
        LoggerInterface $logger,
        $data = []
    ) {
        $this->reminderRepository = $reminderRepository;
        $this->collectionFactory = $collectionFactory;
        $this->logger = $logger;
        $this->data = $data;
    }

    public function execute()
    {
        foreach ($this->collectionFactory->create()->execute() as $item) {
            try {
                $this->reminderRepository->send($item);
            } catch (\Exception $exc) {
                $this->logger->critical($exc);
            }
        }
    }
}
