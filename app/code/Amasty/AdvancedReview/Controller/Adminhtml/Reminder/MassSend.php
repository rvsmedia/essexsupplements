<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2019 Amasty (https://www.amasty.com)
 * @package Amasty_AdvancedReview
 */


namespace Amasty\AdvancedReview\Controller\Adminhtml\Reminder;

use Amasty\AdvancedReview\Api\Data\ReminderInterface;
use Amasty\AdvancedReview\Model\OptionSource\Reminder\Status;
use Magento\Framework\Exception\LocalizedException;

class MassSend extends \Amasty\AdvancedReview\Controller\Adminhtml\AbstractMassAction
{
    /**
     * @param ReminderInterface $reminder
     *
     * @throws LocalizedException
     */
    protected function itemAction(ReminderInterface $reminder)
    {
        $result = $this->repository->send($reminder);
        if (!$result) {
            throw new LocalizedException(__('We can\'t send the email right now. Please check log files'));
        }
    }

    /**
     * @param int $collectionSize
     *
     * @return \Magento\Framework\Phrase
     */
    protected function getSuccessMessage($collectionSize = 0)
    {
        if ($collectionSize) {
            return __('A total of %1 email(s) have been sent.', $collectionSize);
        }

        return __('No records have been sent.');
    }
}
