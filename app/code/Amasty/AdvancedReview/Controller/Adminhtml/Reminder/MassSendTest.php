<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2019 Amasty (https://www.amasty.com)
 * @package Amasty_AdvancedReview
 */


namespace Amasty\AdvancedReview\Controller\Adminhtml\Reminder;

use Amasty\AdvancedReview\Api\Data\ReminderInterface;
use Amasty\AdvancedReview\Model\OptionSource\Reminder\Status;
use Magento\Framework\Exception\LocalizedException;

class MassSendTest extends \Amasty\AdvancedReview\Controller\Adminhtml\AbstractMassAction
{
    /**
     * @param ReminderInterface $reminder
     *
     * @throws LocalizedException
     */
    protected function itemAction(ReminderInterface $reminder)
    {
        if (!$this->config->getTestEmail()) {
            throw new LocalizedException(__('Please fill test email in module configuration'));
        }
        $this->repository->send($reminder, true);
    }

    /**
     * @param int $collectionSize
     *
     * @return \Magento\Framework\Phrase
     */
    protected function getSuccessMessage($collectionSize = 0)
    {
        if ($collectionSize) {
            return __(
                'A total of %1 email(s) have been sent to %2.',
                $collectionSize,
                $this->config->getTestEmail()
            );
        }

        return __('No records have been sent.');
    }
}
