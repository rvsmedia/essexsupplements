<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2019 Amasty (https://www.amasty.com)
 * @package Amasty_AdvancedReview
 */


namespace Amasty\AdvancedReview\Plugin\Review\Model\Adminhtml;

use Amasty\AdvancedReview\Helper\Config;
use Magento\Review\Model\Review as MagentoReview;

class Review
{
    /**
     * @var Config
     */
    private $configHelper;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    private $logger;

    /**
     * @var \Magento\Customer\Api\CustomerRepositoryInterface
     */
    private $customerRepository;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var \Magento\Framework\Mail\Template\TransportBuilder
     */
    private $transportBuilder;

    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    private $request;

    public function __construct(
        Config $configHelper,
        \Magento\Framework\App\RequestInterface $request,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder
    ) {
        $this->configHelper = $configHelper;
        $this->logger = $logger;
        $this->customerRepository = $customerRepository;
        $this->storeManager = $storeManager;
        $this->transportBuilder = $transportBuilder;
        $this->request = $request;
    }

    /**
     * @param MagentoReview $subject
     * @param $result
     *
     * @return mixed
     */
    public function afterAggregate(
        MagentoReview $subject,
        $result
    ) {
        $this->sendAdminReplyToCustomer($subject);

        return $result;
    }

    /**
     * @param MagentoReview $subject
     */
    private function sendAdminReplyToCustomer(MagentoReview $subject)
    {
        $isNeedSendNotification = (int)$this->request->getParam('is_need_send_notification');
        if ($isNeedSendNotification && $subject->getAnswer()) {
            $customerName = '';
            $customerId = $subject->getCustomerId();

            try {
                $customer = $this->customerRepository->getById($customerId);
                $emailTo = $customer->getEmail();
                $customerName = $customer->getFirstname();
            } catch (\Exception $ex) {
                $emailTo = null;
            }

            $guestEmail = $subject->getData('guest_email');
            if (!$emailTo && $guestEmail) {
                $emailTo = $guestEmail;
            }

            if (!$emailTo) {
                return;
            }

            $sender = $this->configHelper->getModuleConfig('customer_notify/sender');
            $template = $this->configHelper->getModuleConfig('customer_notify/template');

            try {
                $store = $this->storeManager->getStore($subject->getStoreId());

                $productUrl = '';
                if ($subject->getStatusId() == MagentoReview::STATUS_APPROVED) {
                    $productUrl = $subject->getProductUrl($subject->getEntityPkValue(), $subject->getStoreId());
                    $productUrl = explode('?', $productUrl);
                    $productUrl = array_shift($productUrl) . '#reviews'; //remove backend params( SID for example)
                }

                $data =  [
                    'website_name'  => $store->getWebsite()->getName(),
                    'group_name'    => $store->getGroup()->getName(),
                    'store_name'    => $store->getName(),
                    'link'          => $productUrl,
                    'admin_answer'  => $subject->getAnswer(),
                    'customer_name' => $customerName,
                ];

                $transport = $this->transportBuilder->setTemplateIdentifier(
                    $template
                )->setTemplateOptions(
                    ['area' => \Magento\Framework\App\Area::AREA_FRONTEND, 'store' => $store->getId()]
                )->setTemplateVars(
                    $data
                )->setFrom(
                    $sender
                )->addTo(
                    $emailTo
                )->getTransport();

                $transport->sendMessage();
            } catch (\Exception $e) {
                $this->logger->critical($e);
            }
        }
    }

    /**
     * @param MagentoReview $subject
     */
    public function beforeAfterSave(MagentoReview $subject)
    {
        if ($this->configHelper->isProsConsEnabled()) {
            $connection = $subject->getResource()->getConnection();
            $reviewDetailTable = $subject->getResource()->getTable('review_detail');

            /* save details */
            $select = $connection->select()->from($reviewDetailTable, 'detail_id')
                ->where('review_id = :review_id');
            $detailId = $connection->fetchOne($select, [':review_id' => $subject->getId()]);

            if ($detailId) {
                $detail = [
                    'like_about'     => $subject->getLikeAbout(),
                    'not_like_about' => $subject->getNotLikeAbout()
                ];

                $condition = ["detail_id = ?" => $detailId];
                $connection->update($reviewDetailTable, $detail, $condition);
            }
        }
    }
}
