<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2019 Amasty (https://www.amasty.com)
 * @package Amasty_AdvancedReview
 */


namespace Amasty\AdvancedReview\Plugin\Review\Block\Adminhtml\Edit;

use Amasty\AdvancedReview\Model\Sources\Recommend;
use Amasty\AdvancedReview\Model\Repository\VoteRepository;
use Magento\Review\Block\Adminhtml\Edit\Form as MagentoForm;

class Form
{
    /**
     * @var \Amasty\AdvancedReview\Helper\Config
     */
    private $configHelper;

    /**
     * @var \Magento\Framework\Registry
     */
    private $registry;

    /**
     * @var VoteRepository
     */
    private $voteRepository;

    /**
     * @var \Amasty\AdvancedReview\Helper\BlockHelper
     */
    private $blockHelper;

    /**
     * @var Recommend
     */
    private $recommendModel;

    public function __construct(
        \Amasty\AdvancedReview\Helper\Config $configHelper,
        \Amasty\AdvancedReview\Helper\BlockHelper $blockHelper,
        \Magento\Framework\Registry $registry,
        VoteRepository $voteRepository,
        Recommend $recommendModel
    ) {
        $this->configHelper = $configHelper;
        $this->registry = $registry;
        $this->voteRepository = $voteRepository;
        $this->blockHelper = $blockHelper;
        $this->recommendModel = $recommendModel;
    }

    /**
     * @param MagentoForm $subject
     * @param \Magento\Framework\Data\Form $form
     * @return array
     */
    public function beforeSetForm(
        MagentoForm $subject,
        \Magento\Framework\Data\Form $form
    ) {
        $fieldset = $form->getElement('review_details');
        $review = $this->registry->registry('review_data');

        $guestEmail = $review->getData('guest_email');
        if ($guestEmail) {
            $text = $form->getElement('customer')->getText();
            $text .= sprintf(' (<a href="mailto:%1$s">%1$s</a>)', $guestEmail);
            $form->getElement('customer')->setText($text);
        }

        if ($this->configHelper->isAllowAnswer()) {
            $fieldsetAnswer = $form->addFieldset(
                'review_answer',
                ['legend' => __('Admin Answer Section'), 'class' => 'fieldset-wide', 'collapsble' => true]
            );

            $field = $fieldsetAnswer->addField(
                'answer',
                'textarea',
                ['label' => __('Answer'), 'required' => false, 'name' => 'answer', 'style' => 'height:24em;']
            );

            $field->setValue($review->getAnswer());

            if ($review->getCustomerId() || $guestEmail) {
                $checkbox = $fieldsetAnswer->addField(
                    'is_need_send_notification',
                    'checkbox',
                    [
                        'label' => __('Send to Customer'),
                        'required' => false,
                        'name' => 'is_need_send_notification'
                    ]
                );

                $checkbox->setValue(1);
            }
        }

        if ($this->configHelper->isRecommendFieldEnabled()) {
            if ($fieldset) {
                $field = $fieldset->addField(
                    'is_recommended',
                    'select',
                    [
                        'label' =>__('I recommend this product'),
                        'required' => false,
                        'values' => $this->recommendModel->toOptionArray(),
                        'name' => 'is_recommended'
                    ]
                );
                $field->setValue((int)$review->getData('is_recommended'));
            }
        }

        if ($this->configHelper->isAllowImages()) {
            if ($fieldset) {
                $fieldset->addField(
                    'images',
                    'note',
                    [
                        'label' => __('Customer Images'),
                        'required' => false,
                        'name' => 'images',
                        'after_element_html' => $this->blockHelper->getReviewImagesHtml($review->getId())
                    ]
                );
            }
        }

        $fieldset->addField(
            'verified_buyer',
            'note',
            [
                'label' => __('Verified Buyer'),
                'required' => false,
                'name' => 'verified_buyer',
                'note' => __('A client who already purchased the product.'),
                'text' => $review->getVerifiedBuyer() ? __('Yes') : __('No'),
            ]
        );

        $fieldset->addField(
            'helpfulness',
            'note',
            [
                'label' => __('Helpfulness'),
                'required' => false,
                'name' => 'Helpfulness',
                'text' => $this->getReviewHelpfulness($review)
            ]
        );

        if ($this->configHelper->isProsConsEnabled()) {
            $like = $fieldset->addField(
                'like_about',
                'textarea',
                [
                    'label'    => __('Advantages'),
                    'required' => false,
                    'name'     => __('like_about')
                ]
            );

            $like->setValue($review->getData('like_about'));

            $disLike = $fieldset->addField(
                'not_like_about',
                'textarea',
                [
                    'label'    => __('Disadvantages'),
                    'required' => false,
                    'name'     => __('not_like_about')
                ]
            );
            $disLike->setValue($review->getData('not_like_about'));
        }

        return [$form];
    }

    /**
     * @param $review
     * @return \Magento\Framework\Phrase
     */
    private function getReviewHelpfulness($review)
    {
        $vote = $this->voteRepository->getVotesCount($review->getId());
        return __('%1 people found this helpful; %2 found this unhelpful.', $vote['plus'], $vote['minus']);
    }
}
