<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2019 Amasty (https://www.amasty.com)
 * @package Amasty_AdvancedReview
 */


namespace Amasty\AdvancedReview\Plugin\Review\Block\Product\View;

use Magento\Review\Block\Product\View\ListView as MagentoListView;
use Magento\Review\Model\ResourceModel\Review\Collection as ReviewCollection;

class ListView
{
    const MIN_LIMIT_VALUE = 5;

    /**
     * @param MagentoListView $subject
     *
     * @return array
     */
    public function beforeGetReviewsCollection(
        MagentoListView $subject
    ) {
        $toolbar = $subject->getLayout()->getBlock('product_review_list.toolbar');
        if ($toolbar) {
            $limit = $toolbar->getAvailableLimit();
            $limit[self::MIN_LIMIT_VALUE] = self::MIN_LIMIT_VALUE;
            ksort($limit);
            $toolbar->setAvailableLimit($limit);
        }

        return [];
    }

    /**
     * @param MagentoListView $subject
     * @param ReviewCollection $reviewCollection
     *
     * @return ReviewCollection
     */
    public function afterGetReviewsCollection(
        MagentoListView $subject,
        ReviewCollection $reviewCollection
    ) {
        if (($stars = (int)$subject->getRequest()->getParam('stars'))
            && !$reviewCollection->getFlag('filter_by_stars')
        ) {
            $reviewCollection->setFlag('filter_by_stars', $stars);
            $reviewCollection->getSelect()->having('rating_summary = ?', $stars);
        }

        return $reviewCollection;
    }
}
