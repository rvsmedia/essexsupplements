<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2019 Amasty (https://www.amasty.com)
 * @package Amasty_AdvancedReview
 */


namespace Amasty\AdvancedReview\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class UpgradeSchema implements UpgradeSchemaInterface
{
    /**
     * @var UpgradeSchema\AddReminderTable
     */
    private $addReminderTable;

    /**
     * @var UpgradeSchema\AddUnsubscribeTable
     */
    private $addUnsubscribeTable;

    /**
     * @var UpgradeSchema\AddProsConsEmailFields
     */
    private $addProsConsEmailFields;

    /**
     * @var UpgradeSchema\UpdateImageTable
     */
    private $updateImageTable;

    public function __construct(
        \Amasty\AdvancedReview\Setup\UpgradeSchema\AddReminderTable $addReminderTable,
        \Amasty\AdvancedReview\Setup\UpgradeSchema\AddUnsubscribeTable $addUnsubscribeTable,
        \Amasty\AdvancedReview\Setup\UpgradeSchema\AddProsConsEmailFields $addProsConsEmailFields,
        UpgradeSchema\UpdateImageTable $updateImageTable
    ) {
        $this->addReminderTable = $addReminderTable;
        $this->addUnsubscribeTable = $addUnsubscribeTable;
        $this->addProsConsEmailFields = $addProsConsEmailFields;
        $this->updateImageTable = $updateImageTable;
    }

    /**
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        if (version_compare($context->getVersion(), '1.2.0', '<')) {
            $this->addReminderTable->execute($setup);
            $this->addUnsubscribeTable->execute($setup);
        }

        if (version_compare($context->getVersion(), '1.3.0', '<')) {
            $this->addProsConsEmailFields->execute($setup);
        }

        if (version_compare($context->getVersion(), '1.4.1', '<')) {
            $this->updateImageTable->execute($setup);
        }

        $setup->endSetup();
    }
}
