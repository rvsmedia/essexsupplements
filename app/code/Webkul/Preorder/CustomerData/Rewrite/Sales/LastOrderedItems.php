<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Preorder
 * @author    Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\Preorder\CustomerData\Rewrite\Sales;

use Magento\Framework\App\ObjectManager;
use Webkul\Preorder\Helper\Data;

/**
 * Cart source
 */
class LastOrderedItems extends \Magento\Sales\CustomerData\LastOrderedItems
{
    /**
     * Get list of last ordered products
     *
     * @return array
     */
    protected function getItems()
    {
        $items = [];
        $order = $this->getLastOrder();
        $limit = $this::SIDEBAR_ORDER_LIMIT;

        if ($order) {
            $preorderHelper = ObjectManager::getInstance()->get(Data::class);
            $items = $preorderHelper->getItems($order, $limit);
        }
        return $items;
    }
}
