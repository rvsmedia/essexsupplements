/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Preorder
 * @author    Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
define([
"jquery",
"jquery/ui",
], function ($) {
    'use strict';
    $.widget('preorder.items', {
        _create: function () {
            var self = this;
            $(document).ready(function () {
                var info = self.options.info;
                if (self.options.display_msg) {
                    var msgBox = $('<div/>').addClass("wk-msg-box wk-info").text(self.options.msg);
                    $("#container").prepend(msgBox);
                }
                var count = 0;
                $(".edit-order-table > tbody").each(function () {
                    $(this).find("tr td:first-child").append(info[count]['preorder_option_html']);
                    count++;
                });
            });
        }
    });
    return $.preorder.items;
});