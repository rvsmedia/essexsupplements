/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Preorder
 * @author    Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
var config = {
    map: {
        '*': {
            items: 'Webkul_Preorder/js/items',
            list: 'Webkul_Preorder/js/list',
            view: 'Webkul_Preorder/js/view',
            compare: 'Webkul_Preorder/js/compare',
        }
    }
};
