/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Preorder
 * @author    Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
define([
"jquery",
"jquery/ui",
], function ($) {
    'use strict';
    $.widget('preorder.view', {
        options: {},
        _create: function () {
            var self = this;
            $(document).ready(function () {
                var url = self.options.url;
                var payHtml = self.options.payHtml;
                var msg = self.options.msg;
                var flag = self.options.flag;
                var productId = self.options.productId;
                var addToCartButtonLabel = $("#product-addtocart-button span").text();
                var stockLabel = $(".product-info-stock-sku .stock").text();
                var preOrderLabel = self.options.preOrderLabel;
                var outOfStockLabel = self.options.outOfStockLabel;
                msg = msg.replace(/\n/g, "<br />");
                var count = 0;
                var isPreorder = flag;
                var boxHtml = $(".box-tocart");
                if (isPreorder == 1) {
                    setPreOrderLabel();
                    $(".product-info-price").after(msg);
                    $(".product-info-price").after(payHtml);
                }
                $('#product-addtocart-button').click(function () {
                    count = 0;
                });
                $('#product-addtocart-button span').bind("DOMSubtreeModified",function () {
                    var title = $(this).text();
                    if (isPreorder == 1) {
                        if (title == addToCartButtonLabel) {
                            count++;
                            if (count == 1) {
                                setPreOrderLabel();
                            }
                        }
                    }
                });
                $('#product-options-wrapper .super-attribute-select').change(function () {
                    $(".wk-msg-box").remove();
                    var flag = 1;
                    setTimeout(function () {
                        $("#product_addtocart_form input[type='hidden']").each(function () {
                            $('#product-options-wrapper .super-attribute-select').each(function () {
                                if ($(this).val() == "") {
                                    flag = 0;
                                }
                            });
                            var name = $(this).attr("name");
                            if (name == "selected_configurable_option") {
                                var productId = $(this).val();
                                if (productId != "" && flag ==1) {
                                    $(".wk-loading-mask").removeClass("wk-display-none");
                                    $.ajax({
                                        url: url,
                                        type: 'POST',
                                        data: { product_id : productId },
                                        dataType: 'json',
                                        success: function (data) {
                                            manageAddToCart(data);
                                            if (data.preorder == 1) {
                                                isPreorder = 1;
                                                $(".product-info-price").after(data.msg);
                                                $(".product-info-price").after(data.payHtml);
                                            } else {
                                                isPreorder = 0;
                                            }
                                            manageLabel(data);
                                            $(".wk-loading-mask").addClass("wk-display-none");
                                        }
                                    });
                                }
                            }
                        });
                    }, 0);
                });
                $('body').on('click', '#product-options-wrapper .swatch-option', function () {
                    var flag = 1;
                    var attributeInfo = {};
                    $(".wk-msg-box").remove();
                    setTimeout(function () {
                        $('#product-options-wrapper .swatch-attribute').each(function () {
                            if ($(this).attr('option-selected')) {
                                var selectedOption = $(this).attr("option-selected");
                                var attributeId = $(this).attr("attribute-id");
                                attributeInfo[attributeId] = selectedOption;
                            } else {
                                flag = 0;
                            }
                        });
                        if (flag == 1) {
                            $(".wk-loading-mask").removeClass("wk-display-none");
                            $.ajax({
                                url: url,
                                type: 'POST',
                                data: { type : 1, product_id : productId, info : attributeInfo },
                                dataType: 'json',
                                success: function (data) {
                                    manageAddToCart(data);
                                    if (data.preorder == 1) {
                                        isPreorder = 1;
                                        $(".product-info-price").after(data.msg);
                                        $(".product-info-price").after(data.payHtml);
                                    } else {
                                        isPreorder = 0;
                                    }
                                    manageLabel(data);
                                    $(".wk-loading-mask").addClass("wk-display-none");
                                }
                            });
                        }
                    }, 0);
                });

                

                function setPreOrderLabel()
                {
                    $("#product-addtocart-button").attr("title",preOrderLabel);
                    $("#product-addtocart-button span").text(preOrderLabel);
                    $(".product-info-stock-sku .stock").text(preOrderLabel);
                }

                function setDefaultLabel()
                {
                    $("#product-addtocart-button").attr("title",addToCartButtonLabel);
                    $("#product-addtocart-button span").text(addToCartButtonLabel);
                    $(".product-info-stock-sku .stock").text(stockLabel);
                }

                function setOutOfStockLabel()
                {
                    $(".product-info-stock-sku .stock").text(outOfStockLabel);
                }

                function manageLabel(data)
                {
                    if (data.preorder == 1) {
                        setPreOrderLabel();
                    } else {
                        if (data.stock.is_in_stock == 1) {
                            setDefaultLabel();
                        } else {
                            setOutOfStockLabel();
                        }
                    }
                }

                function removeBox()
                {
                    $(".box-tocart").remove();
                }

                function addBox()
                {
                    $(".product-options-bottom").append(boxHtml);
                }

                function manageAddToCart(data)
                {
                    removeBox();
                    if (data.preorder == 1) {
                        addBox();
                    } else {
                        if (data.stock.is_in_stock == 1) {
                            addBox();
                        }
                    }
                }
            });
        }
    });
    return $.preorder.view;
});