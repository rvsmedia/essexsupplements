<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Preorder
 * @author    Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\Preorder\Controller\Preorder;

use Magento\Framework\App\Action\Context;

class Check extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Webkul\Preorder\Helper\Data
     */
    protected $_preorderHelper;

    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    protected $_resultJsonFactory;

    /**
     * @param Context $context
     * @param \Webkul\Preorder\Helper\Data $preorderHelper
     * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
     */
    public function __construct(
        Context $context,
        \Webkul\Preorder\Helper\Data $preorderHelper,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
    ) {
        $this->_preorderHelper = $preorderHelper;
        $this->_resultJsonFactory = $resultJsonFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $info = [];
        $helper = $this->_preorderHelper;
        $type = $this->getRequest()->getParam('type');
        $productId = $this->getRequest()->getParam('product_id');
        if ($type == 1) {
            $product = $this->_preorderHelper->getProduct($productId);
            $attributesInfo = $this->getRequest()->getParam('info');
            $productId = $helper->getAssociatedId($attributesInfo, $product);
        }

        if ($helper->isPreorder($productId)) {
            $payHtml = $helper->getPayPreOrderHtml();
            $msg = $helper->getPreOrderInfoBlock($productId);
            $info['preorder'] = 1;
            $info['msg'] = $msg;
            $info['payHtml'] = $payHtml;
        } else {
            $info['preorder'] = 0;
        }
        $info['stock'] = $helper->getStockDetails($productId);

        $result = $this->_resultJsonFactory->create();
        $result->setData($info);
        return $result;
    }
}
