<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Preorder
 * @author    Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\Preorder\Controller\Adminhtml\Preorder;

use Magento\Backend\App\Action\Context;
use Magento\Ui\Component\MassAction\Filter;
use Webkul\Preorder\Model\ResourceModel\Preorder\CollectionFactory;
use Webkul\Preorder\Helper\Data as PreorderHelper;

class MassEmail extends \Magento\Backend\App\Action
{
    /**
     * @var Filter
     */
    protected $_filter;

    /**
     * @var CollectionFactory
     */
    protected $_collectionFactory;

    /**
     * @param Context $context
     * @param Filter $filter
     * @param CollectionFactory $collectionFactory
     */
    public function __construct(
        Context $context,
        Filter $filter,
        CollectionFactory $collectionFactory,
        \Webkul\Preorder\Helper\Data $preorderHelper
    ) {
        $this->_filter = $filter;
        $this->_collectionFactory = $collectionFactory;
        $this->_preorderHelper = $preorderHelper;
        parent::__construct($context);
    }

    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Webkul_Preorder::preorder');
    }

    /**
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $helper = $this->_preorderHelper;
        $collection = $this->_filter->getCollection($this->_collectionFactory->create());
        $info = [];
        $ids = [];
        foreach ($collection as $item) {
            if ($item->getPreorderStatus() == PreorderHelper::PREORDER_STATUS_PENDING) {
                $info[] = $item->getProductId();
                $ids[] = $item->getId();
            }
        }
        $info = array_unique($info);
        try {
            $helper->massNotify($info, $ids);
            $this->messageManager->addSuccess(__('Email sent succesfully.'));
        } catch (\Exception $e) {
            $this->messageManager->addError(__('Something went wrong.'));
        }

        $resultRedirect = $this->resultRedirectFactory->create();
        return $resultRedirect->setPath('*/*/');
    }
}
