<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Preorder
 * @author    Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\Preorder\Controller\Rewrite\Adminhtml\Order\Shipment;

class Save extends \Magento\Shipping\Controller\Adminhtml\Order\Shipment\Save
{
    public function execute()
    {
        $helper = $this->helper();
        $result = $helper->checkShipment();
        if ($result['error']) {
            $resultRedirect = $this->resultRedirectFactory->create();
            $this->messageManager->addError($result['msg']);
            $params = [
                        '_current' => true,
                        'order_id' => $this->getRequest()->getParam("order_id")
                    ];
            return $resultRedirect->setPath('sales/order/view', $params);
        }

        return parent::execute();
    }

    public function helper()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $helper = $objectManager->create('Webkul\Preorder\Helper\Data');
        return $helper;
    }
}
