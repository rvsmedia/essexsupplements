<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Preorder
 * @author    Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\Preorder\Plugin\CatalogInventory\Model\Stock;

use Magento\CatalogInventory\Model\Stock\Status as StockStatus;

class Status
{
    /**
     * @var \Webkul\Preorder\Helper\Data
     */
    private $_preorderHelper;

    /**
     * Initialize dependencies.
     *
     * @param \Webkul\Preorder\Helper\Data $preorderHelper
     */
    public function __construct(
        \Magento\Framework\Registry $coreRegistry,
        \Webkul\Preorder\Helper\Data $preorderHelper
    ) {
        $this->_coreRegistry = $coreRegistry;
        $this->_preorderHelper = $preorderHelper;
    }

    public function afterGetStockStatus(
        StockStatus $subject,
        $result
    ) {
        $productId = $subject->getProductId();
        $helper = $this->_preorderHelper;
        if ($helper->isPreorder($productId)) {
            return true;
        } elseif ($helper->isConfigPreorder($productId)) {
            return true;
        } else {
            return $result;
        }
    }
}
