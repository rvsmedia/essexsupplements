<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Preorder
 * @author    Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\Preorder\Plugin\OfflineShipping\Model\Carrier;

use Magento\OfflineShipping\Model\Carrier\Tablerate as MagentoTableRate;

class Tablerate
{
    /**
     * @var \Webkul\Preorder\Helper\Data
     */
    private $_preorderHelper;

    /**
     * Initialize dependencies.
     *
     * @param \Webkul\Preorder\Helper\Data $preorderHelper
     */
    public function __construct(
        \Webkul\Preorder\Helper\Data $preorderHelper
    ) {
        $this->_preorderHelper = $preorderHelper;
    }

    public function beforeCollectRates(
        MagentoTableRate $subject,
        $request
    ) {
        $finalPrice = 0;
        foreach ($request->getAllItems() as $item) {
            $finalPrice += $this->_preorderHelper->getTotalOriginalPriceByItem($item);
        }

        $request->setPackageValue($finalPrice);
        return [$request];
    }
}
