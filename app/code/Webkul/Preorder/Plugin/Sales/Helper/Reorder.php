<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Preorder
 * @author    Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\Preorder\Plugin\Sales\Helper;

use Magento\Sales\Helper\Reorder as SalesReorder;

class Reorder
{
    /**
     * @var \Webkul\Preorder\Helper\Data
     */
    private $_preorderHelper;

    /**
     * Initialize dependencies.
     *
     * @param \Webkul\Preorder\Helper\Data $preorderHelper
     */
    public function __construct(
        \Webkul\Preorder\Helper\Data $preorderHelper
    ) {
        $this->_preorderHelper = $preorderHelper;
    }

    public function aroundCanReorder(
        SalesReorder $subject,
        \Closure $proceed,
        $orderId
    ) {
        $helper = $this->_preorderHelper;
        if (!$this->_preorderHelper->canReorder($orderId)) {
            return false;
        }

        $result = $proceed($orderId);
        return $result;
    }
}
