<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Preorder
 * @author    Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\Preorder\Plugin\Catalog\Helper;

use Magento\Catalog\Helper\Product as ProductHelper;

class Product
{
    /**
     * @var \Webkul\Preorder\Helper\Data
     */
    private $_preorderHelper;

    /**
     * Initialize dependencies.
     *
     * @param \Webkul\Preorder\Helper\Data $preorderHelper
     */
    public function __construct(
        \Magento\Framework\Registry $coreRegistry,
        \Webkul\Preorder\Helper\Data $preorderHelper
    ) {
        $this->_coreRegistry = $coreRegistry;
        $this->_preorderHelper = $preorderHelper;
    }

    public function afterGetSkipSaleableCheck(
        ProductHelper $subject,
        $result
    ) {
        if ($currentProduct = $this->_coreRegistry->registry('current_product')) {
            if ($this->_preorderHelper->isConfigPreorder($currentProduct->getId())) {
                return true;
            }
        }

        return $result;
    }
}
