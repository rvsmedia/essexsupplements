<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Preorder
 * @author    Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\Preorder\Plugin\Catalog\Helper\Product;

use Magento\Catalog\Model\Product\Configuration\Item\ItemInterface;
use Magento\Catalog\Helper\Product\Configuration as ProductConfiguration;

class Configuration
{
    /**
     * @var \Webkul\Preorder\Helper\Data
     */
    private $_preorderHelper;

    /**
     * Initialize dependencies.
     *
     * @param \Webkul\Preorder\Helper\Data $preorderHelper
     */
    public function __construct(
        \Webkul\Preorder\Helper\Data $preorderHelper
    ) {
        $this->_preorderHelper = $preorderHelper;
    }

    public function aroundGetCustomOptions(
        ProductConfiguration $subject,
        \Closure $proceed,
        ItemInterface $item
    ) {
        $helper = $this->_preorderHelper;
        $result = $proceed($item);
        $price = $helper->getOriginalPriceByItem($item);
        $price = $helper->getPriceWithCurrency($price);
        $productId = $item->getProduct()->getId();
        if ($item->getChildren()) {
            foreach ($item->getChildren() as $value) {
                $productId = $value->getProductId();
            }
        }

        $preordertype = $helper->getPreorderType() ?  "Partial Preorder" : "Complete Payment Preorder";
        $preorderPercent = $helper->getPreorderPercent();
        if ($helper->isPreorder($productId)) {
            $result[] = [
                        "label" => __("Preorder Type"),
                        "value" => __($preordertype)
                    ];
            if ($helper->isPartialPreorder($productId)) {
                $result[] = [
                            "label" => __("Preorder Percent"),
                            "value" => __($preorderPercent)
                        ];
                $result[] = [
                            "label" => __("Original Price"),
                            "value" => __($price)
                        ];
            }
        }

        foreach ($result as $key => $option) {
            if ($option['label'] == "Order Refernce") {
                $url = $helper->getOrderViewUrl($option['value'], false);
                $result[$key]['value'] = "<a target='_blank' href='".$url."'>".$option['value']."</a>";
            }

            if ($option['label'] == "Options Info") {
                unset($result[$key]);
            }
        }

        return $result;
    }
}
