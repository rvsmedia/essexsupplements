<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Preorder
 * @author    Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\Preorder\Plugin\ConfigurableProduct\Model\ResourceModel\Attribute;

use Magento\ConfigurableProduct\Model\ResourceModel\Attribute\OptionSelectBuilderInterface as OptionSelectBuilder;

class OptionSelectBuilderInterface
{
    /**
     * @var \Webkul\Preorder\Helper\Data
     */
    private $_preorderHelper;

    /**
     * Initialize dependencies.
     *
     * @param \Webkul\Preorder\Helper\Data $preorderHelper
     */
    public function __construct(
        \Magento\Framework\Registry $coreRegistry,
        \Webkul\Preorder\Helper\Data $preorderHelper
    ) {
        $this->_coreRegistry = $coreRegistry;
        $this->_preorderHelper = $preorderHelper;
    }

    public function afterGetSelect(
        OptionSelectBuilder $subject,
        $result
    ) {
        if ($currentProduct = $this->_coreRegistry->registry('current_product')) {
            if ($this->_preorderHelper->isConfigPreorder($currentProduct->getId())) {
                $result = str_replace('stock.stock_status = 1', 'stock.stock_status >= 0', $result);
            }
        }

        return $result;
    }
}
