<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Preorder
 * @author    Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\Preorder\Plugin\Checkout\Controller\Cart;

use Magento\Checkout\Controller\Cart\Configure as CartConfigure;
use Magento\Framework\App\Response\Http as responseHttp;
use Magento\Framework\UrlInterface;

class Configure
{
    /**
     * @var \Webkul\Preorder\Helper\Data
     */
    private $_preorderHelper;

    /**
     * Initialize dependencies.
     *
     * @param \Webkul\Preorder\Helper\Data $preorderHelper
     */
    public function __construct(
        \Webkul\Preorder\Helper\Data $preorderHelper,
        responseHttp $response,
        UrlInterface $url,
        \Magento\Framework\Message\ManagerInterface $messageManager
    ) {
        $this->response = $response;
        $this->_url = $url;
        $this->_preorderHelper = $preorderHelper;
        $this->_messageManager = $messageManager;
    }

    public function afterExecute(
        CartConfigure $subject,
        $result
    ) {
        $productId = (int)$subject->getRequest()->getParam('product_id');
        if ($productId == $this->_preorderHelper->getPreorderCompleteProductId()) {
            $url = $this->_url->getUrl('checkout/cart');
            $this->response->setRedirect($url);
            $this->_messageManager->addError(__("You can not edit Complete PreOrder product's parameters."));
            return $result;
        }

        return $result;
    }
}
