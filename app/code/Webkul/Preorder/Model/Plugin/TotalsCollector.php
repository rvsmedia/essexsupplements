<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Preorder
 * @author    Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\Preorder\Model\Plugin;

use Magento\Quote\Model\Quote\TotalsCollector as Collector;

class TotalsCollector
{
    /**
     * @var \Webkul\Preorder\Helper\Data
     */
    private $_preorderHelper;

    /**
     * Initialize dependencies.
     *
     * @param \Webkul\Preorder\Helper\Data $preorderHelper
     */
    public function __construct(
        \Webkul\Preorder\Helper\Data $preorderHelper
    ) {
        $this->_preorderHelper = $preorderHelper;
    }

    public function aroundCollect(
        Collector $subject,
        \Closure $proceed,
        \Magento\Quote\Model\Quote $quote
    ) {
        $this->_preorderHelper->collectTotals($quote);
        $result = $proceed($quote);
        return $result;
    }
}
