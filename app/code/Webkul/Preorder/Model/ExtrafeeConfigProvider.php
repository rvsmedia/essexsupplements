<?php
namespace Webkul\Preorder\Model;

use Magento\Checkout\Model\ConfigProviderInterface;

class ExtrafeeConfigProvider implements ConfigProviderInterface
{
    /**
     * @var \Webkul\Preorder\Helper\Data
     */
    protected $dataHelper;

    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $checkoutSession;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    /**
     * @param \Webkul\Preorder\Helper\Data $dataHelper
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param \Psr\Log\LoggerInterface $logger
     */
    public function __construct(
        \Webkul\Preorder\Helper\Data $dataHelper,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Psr\Log\LoggerInterface $logger
    ) {
        $this->dataHelper = $dataHelper;
        $this->checkoutSession = $checkoutSession;
        $this->logger = $logger;
    }

    /**
     * @return array
     */
    public function getConfig()
    {
        $extrafeeConfig = [];
        $extrafeeConfig['fee_label'] = __('Preorder Fee');
        $quote = $this->checkoutSession->getQuote();
        $subtotal = $quote->getSubtotal();
        $extrafeeConfig['custom_fee_amount'] = $quote->getPreorderFee();
        $extrafeeConfig['show_hide_Extrafee_block'] = ($quote->getPreorderFee()) ? true : false;
        $extrafeeConfig['show_hide_Extrafee_shipblock'] = ($quote->getPreorderFee()) ? true : false;
        return $extrafeeConfig;
    }
}
