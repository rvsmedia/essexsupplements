<?php

/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Preorder
 * @author    Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\Preorder\Model\Invoice\Total;

use Magento\Sales\Model\Order\Invoice\Total\AbstractTotal;

class PreorderFee extends AbstractTotal
{
    /**
     * @param \Magento\Sales\Model\Order\Invoice $invoice
     * @return $this
     */
    public function collect(\Magento\Sales\Model\Order\Invoice $invoice)
    {
        $amount = $invoice->getOrder()->getPreorderFee();
        $invoice->setPreorderFee($amount);

        $invoice->setGrandTotal($invoice->getGrandTotal() + $invoice->getPreorderFee());
        $invoice->setBaseGrandTotal($invoice->getBaseGrandTotal() + $invoice->getPreorderFee());
        return $this;
    }
}
