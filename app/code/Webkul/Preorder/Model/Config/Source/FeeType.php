<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Preorder
 * @author    Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\Preorder\Model\Config\Source;

class FeeType implements \Magento\Framework\Option\ArrayInterface
{
    const PREORDER_FEE_TYPE_FIXED = 'F';

    const PREORDER_FEE_TYPE_PERCENTAGE = 'P';

    /**
     * {@inheritdoc}
     */
    public function toOptionArray()
    {
        return [
            [
                'value' => self::PREORDER_FEE_TYPE_FIXED,
                'label' => __('Fixed'),
            ],
            [
                'value' => self::PREORDER_FEE_TYPE_PERCENTAGE,
                'label' => __('Percent')
            ]
        ];
    }
}
