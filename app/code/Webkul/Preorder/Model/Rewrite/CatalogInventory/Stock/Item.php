<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Preorder
 * @author    Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\Preorder\Model\Rewrite\CatalogInventory\Stock;

class Item extends \Magento\CatalogInventory\Model\Stock\Item
{
    /**
     * Retrieve Stock Availability.
     *
     * @return bool|int
     */
    public function getIsInStock()
    {
        $isInStock = $this->_getData(static::IS_IN_STOCK);
        $productId = $this->getProductId();
        $helper = $this->helper();
        if ($helper->isPreorder($productId, $isInStock)) {
            return true;
        } elseif ($helper->isConfigPreorder($productId, $isInStock)) {
            return true;
        } else {
            if (!$this->getManageStock()) {
                return true;
            }

            return (bool) $this->_getData(static::IS_IN_STOCK);
        }
    }

    public function getQty($custom = false)
    {
        $isInStock = $this->_getData(static::IS_IN_STOCK);
        $productId = $this->getProductId();
        $helper = $this->helper();
        if ($helper->isAdminArea() || $custom) {
            return parent::getQty();
        } else {
            if ($helper->isPreorder($productId, $isInStock)) {
                return 99999;
            } elseif ($helper->isConfigPreorder($productId, $isInStock)) {
                return 99999;
            }
        }
        return parent::getQty();
    }

    public function helper()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $helper = $objectManager->create('Webkul\Preorder\Helper\Data');
        return $helper;
    }
}
