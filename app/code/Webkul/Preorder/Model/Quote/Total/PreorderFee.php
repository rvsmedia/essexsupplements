<?php

/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Preorder
 * @author    Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\Preorder\Model\Quote\Total;

use Magento\Store\Model\ScopeInterface;
use \Webkul\Preorder\Model\Config\Source\FeeType;

class PreorderFee extends \Magento\Quote\Model\Quote\Address\Total\AbstractTotal
{

    protected $helperData;

    protected $_priceCurrency;

    protected $_check = 0;

    protected $quoteValidator = null;

    /**
     * @param \Magento\Quote\Model\QuoteValidator $quoteValidator
     * @param \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency
     * @param \Webkul\Preorder\Helper\Data $helperData
     */
    public function __construct(
        \Magento\Quote\Model\QuoteValidator $quoteValidator,
        \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency,
        \Webkul\Preorder\Helper\Data $helperData
    ) {
        $this->quoteValidator = $quoteValidator;
        $this->_priceCurrency = $priceCurrency;
        $this->helperData = $helperData;
    }

    public function collect(
        \Magento\Quote\Model\Quote $quote,
        \Magento\Quote\Api\Data\ShippingAssignmentInterface $shippingAssignment,
        \Magento\Quote\Model\Quote\Address\Total $total
    ) {
        parent::collect($quote, $shippingAssignment, $total);
        if (!count($shippingAssignment->getItems())) {
            return $this;
        }
        $subtotal = $total->getTotalAmount('subtotal');
        
        $items = $quote->getAllItems();
        foreach ($items as $item) {
            if ($this->helperData->isPreorder($item->getProductId())) {
                $this->_check++;
                if ($this->helperData->getPreorderFeeType() == FeeType::PREORDER_FEE_TYPE_PERCENTAGE) {
                    $product = $this->helperData->getProductByRepository($item->getProductId());
                    $price = $product->getFinalPrice() * $item->getQty();
                }
            }
        }
        $fee = $this->helperData->getPreorderFee();
        if ($this->helperData->getPreorderFeeType() == FeeType::PREORDER_FEE_TYPE_PERCENTAGE
            && $this->_check
        ) {
            $fee = ($fee * $price)/100;
        }
        if ($fee && $this->_check) {
            $total->setTotalAmount('preorder_fee', $fee);
            $total->setBaseTotalAmount('preorder_fee', $fee);
            $total->setPreorderFee($fee);
            $quote->setPreorderFee($fee);
        }
        return $this;
    }

    /**
     * @param \Magento\Quote\Model\Quote $quote
     * @param \Magento\Quote\Model\Quote\Address\Total $total
     * @return array
     */
    public function fetch(\Magento\Quote\Model\Quote $quote, \Magento\Quote\Model\Quote\Address\Total $total)
    {
        $subtotal = $quote->getSubtotal();
        $fee = $quote->getPreorderFee();

        $result = [];
        if ($fee && $this->_check) {
            $result = [
                'code' => 'preorder_fee',
                'title' => $this->getLabel(),
                'value' => $fee
            ];
        }
        return $result;
    }

    /**
     * Get Subtotal label
     *
     * @return \Magento\Framework\Phrase
     */
    public function getLabel()
    {
        return __('Preorder Fee');
    }

    /**
     * @param \Magento\Quote\Model\Quote\Address\Total $total
     */
    protected function clearValues(\Magento\Quote\Model\Quote\Address\Total $total)
    {
        $total->setTotalAmount('subtotal', 0);
        $total->setBaseTotalAmount('subtotal', 0);
        $total->setTotalAmount('tax', 0);
        $total->setBaseTotalAmount('tax', 0);
        $total->setTotalAmount('discount_tax_compensation', 0);
        $total->setBaseTotalAmount('discount_tax_compensation', 0);
        $total->setTotalAmount('shipping_discount_tax_compensation', 0);
        $total->setBaseTotalAmount('shipping_discount_tax_compensation', 0);
        $total->setSubtotalInclTax(0);
        $total->setBaseSubtotalInclTax(0);
    }
}
