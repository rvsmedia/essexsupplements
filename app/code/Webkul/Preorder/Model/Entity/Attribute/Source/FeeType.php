<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Webkul\Preorder\Model\Entity\Attribute\Source;

/**
 * @api
 * @since 100.0.2
 */
class FeeType extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource
{
    const PREORDER_FEE_TYPE_FIXED = 'F';

    const PREORDER_FEE_TYPE_PERCENTAGE = 'P';

    /**
     * Retrieve all options array
     *
     * @return array
     */
    public function getAllOptions()
    {
        if ($this->_options === null) {
            $this->_options = [
                [
                    'value' => self::PREORDER_FEE_TYPE_FIXED,
                    'label' => __('Fixed'),
                ],
                [
                    'value' => self::PREORDER_FEE_TYPE_PERCENTAGE,
                    'label' => __('Percent')
                ]
            ];
        }
        return $this->_options;
    }
}
