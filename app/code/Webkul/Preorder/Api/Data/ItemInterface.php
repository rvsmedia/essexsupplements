<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Preorder
 * @author    Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\Preorder\Api\Data;

interface ItemInterface
{
    /**
     * Constants for keys of data array.
     */
    const ENTITY_ID = 'id';
    /**#@-*/

    /**
     * Get ID.
     *
     * @return int|null
     */
    public function getId();

    /**
     * Set ID.
     *
     * @param int $id
     *
     * @return \Webkul\Preorder\Api\Data\ItemInterface
     */
    public function setId($id);
}
