<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Preorder
 * @author    Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\Preorder\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * @codeCoverageIgnore
 */
class UpgradeSchema implements UpgradeSchemaInterface
{
    /**
     * {@inheritdoc}
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        $setup->getConnection()->addColumn(
            $setup->getTable('wk_preorder_items'),
            'preorder_status',
            [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                'unsigned' => true,
                'nullable' => false,
                'comment' => 'Preorder Status'
            ]
        );
        $setup->getConnection()->addColumn(
            $setup->getTable('wk_preorder_items'),
            'invoice_status',
            [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                'nullable' => false,
                'comment' => 'Invoice Status'
            ]
        );
        $setup->getConnection()->addColumn(
            $setup->getTable('wk_preorder_items'),
            'invoiced_qty',
            [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                'nullable' => false,
                'comment' => 'Invoiced Quantity'
            ]
        );

        $setup->getConnection()->addColumn(
            $setup->getTable('wk_preorder_items'),
            'total_paid_amount',
            [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
                'length'    => '12,4',
                'nullable' => false,
                'comment' => 'Total paid Amount'
            ]
        );
        $setup->getConnection()->addColumn(
            $setup->getTable('wk_preorder_items'),
            'total_remaining_amount',
            [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
                'length'    => '12,4',
                'nullable' => false,
                'comment' => 'Total Remaining Amount'
            ]
        );
        $setup->getConnection()->addColumn(
            $setup->getTable('wk_preorder_items'),
            'parent_item_id',
            [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                'nullable' => false,
                'comment' => 'Parent Item Id'
            ]
        );
        $setup->getConnection()->addColumn(
            $setup->getTable('wk_preorder_items'),
            'tax_percent',
            [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
                'length'    => '12,4',
                'nullable' => false,
                'comment' => 'Tax Percent'
            ]
        );
        $setup->getConnection()->addColumn(
            $setup->getTable('wk_preorder_items'),
            'paid_tax_amount',
            [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
                'length'    => '12,4',
                'nullable' => false,
                'comment' => 'Paid Tax Amount'
            ]
        );
        $setup->getConnection()->addColumn(
            $setup->getTable('wk_preorder_items'),
            'remaining_tax_amount',
            [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
                'length'    => '12,4',
                'nullable' => false,
                'comment' => 'Remaining Tax Amount'
            ]
        );
        $setup->getConnection()->addColumn(
            $setup->getTable('wk_preorder_items'),
            'base_currency_code',
            [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'nullable' => false,
                'comment' => 'Base Currency Code'
            ]
        );
        $setup->getConnection()->addColumn(
            $setup->getTable('wk_preorder_items'),
            'order_currency_code',
            [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'nullable' => false,
                'comment' => 'Order Currency Code'
            ]
        );
        $setup->getConnection()->addColumn(
            $setup->getTable('wk_preorder_quote_item'),
            'status',
            [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                'nullable' => false,
                'comment' => 'Status'
            ]
        );
        if ($setup->getConnection()->tableColumnExists($setup->getTable('wk_preorder_items'), "customer_email")) {
            $setup->getConnection()->changeColumn(
                $setup->getTable('wk_preorder_items'),
                'customer_email',
                'buyer_email',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => 255,
                    'comment' => 'Customer Email',
                    'nullable' => false
                ]
            );
        }

        if ($setup->getConnection()->tableColumnExists($setup->getTable('wk_preorder_items'), "customer_name")) {
            $setup->getConnection()->changeColumn(
                $setup->getTable('wk_preorder_items'),
                'customer_name',
                'buyer_name',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => 255,
                    'comment' => 'Customer Name',
                    'nullable' => false
                ]
            );
        } else {
            $setup->getConnection()->addColumn(
                $setup->getTable('wk_preorder_items'),
                'buyer_name',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'nullable' => false,
                    'comment' => 'Customer Name',
                    'nullable' => false
                ]
            );
        }

        $setup->getConnection()->changeColumn(
            $setup->getTable('wk_preorder_items'),
            'total_paid_amount',
            'total_paid_amount',
            [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
                'length'    => '12,4',
                'nullable' => false,
                'comment' => 'Total paid Amount'
            ]
        );

        $setup->getConnection()->changeColumn(
            $setup->getTable('wk_preorder_items'),
            'total_remaining_amount',
            'total_remaining_amount',
            [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
                'length'    => '12,4',
                'nullable' => false,
                'comment' => 'Total Remaining Amount'
            ]
        );

        $setup->getConnection()->addColumn(
            $setup->getTable('wk_preorder_items'),
            'product_name',
            [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'nullable' => false,
                'comment' => 'Product Name'
            ]
        );
        
        $setup->getConnection()
            ->addColumn(
                $setup->getTable('quote_address'),
                'preorder_fee',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
                    'length'    => '12,4',
                    'default' => 0.00,
                    'nullable' => true,
                    'comment' =>'Preorder Fee'
                ]
            );
       
        //Quote tables
        $setup->getConnection()
            ->addColumn(
                $setup->getTable('quote'),
                'preorder_fee',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
                    'length'    => '12,4',
                    'default' => 0.00,
                    'nullable' => true,
                    'comment' =>'Preorder Fee'
                ]
            );

        //Order tables
        $setup->getConnection()
            ->addColumn(
                $setup->getTable('sales_order'),
                'preorder_fee',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
                    'length'    => '12,4',
                    'default' => 0.00,
                    'nullable' => true,
                    'comment' =>'Preorder Fee'
                ]
            );

        //Invoice tables
        $setup->getConnection()
            ->addColumn(
                $setup->getTable('sales_invoice'),
                'preorder_fee',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
                    'length'    => '12,4',
                    'default' => 0.00,
                    'nullable' => true,
                    'comment' =>'Preorder Fee'
                ]
            );
        //Credit memo tables
        $setup->getConnection()
            ->addColumn(
                $setup->getTable('sales_creditmemo'),
                'preorder_fee',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
                    'length'    => '12,4',
                    'default' => 0.00,
                    'nullable' => true,
                    'comment' =>'Preorder Fee'
                ]
            );

        /**
         * Update tables 'sales_order'
         */
        $setup->getConnection()->addColumn(
            $setup->getTable('sales_order'),
            'order_type',
            [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                'unsigned' => true,
                'nullable' => false,
                'default' => '0',
                'comment' => 'order_type'
            ]
        );

        /**
         * Update tables 'sales_order_grid'
         */
        $setup->getConnection()->addColumn(
            $setup->getTable('sales_order_grid'),
            'order_type',
            [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                'unsigned' => true,
                'nullable' => false,
                'default' => '0',
                'comment' => 'order_type'
            ]
        );

        $setup->endSetup();
        $this->updateOldRecords();
        $this->createOptionsInfoCustomOption();
    }

    private function createOptionsInfoCustomOption()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $state = $objectManager->get('Magento\Framework\App\State');
        $state->setAreaCode('frontend');
        $helper = $objectManager->get('Webkul\Preorder\Helper\Data');
        $helper->createOptionsInfoCustomOption();
    }

    private function updateOldRecords()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $collection = $objectManager->get('Webkul\Preorder\Model\ResourceModel\Preorder\Collection');
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $tableName = $resource->getTableName('sales_order');
        $sql = "order.entity_id = main_table.order_id ";
        $fields = ["order_currency_code", "base_currency_code", "entity_id"];
        $collection->getSelect()->join(['order' => $tableName], $sql, $fields);

        $tableName = $resource->getTableName('sales_order_item');
        $sql = "order_item.item_id = main_table.item_id ";
        $fields = ["name", "item_id"];
        $collection->getSelect()->join(['order_item' => $tableName], $sql, $fields);

        $collection->addFilterToMap("base_currency_code", "order.base_currency_code");
        $collection->addFilterToMap("order_currency_code", "order.order_currency_code");
        foreach ($collection as $item) {
            $qty = $item->getQty();
            $paidAmount = $item->getPaidAmount();
            $remainingAmount = $item->getRemainingAmount();
            $data = [];
            $data['total_paid_amount'] = $paidAmount * $qty;
            $data['total_remaining_amount'] = $remainingAmount * $qty;
            $data['order_currency_code'] = $item->getOrderCurrencyCode();
            $data['base_currency_code'] = $item->getBaseCurrencyCode();
            $data['product_name'] = $item->getName();
            $this->updateItem($item, $data);
        }
    }

    private function updateItem($item, $data)
    {
        $item->addData($data)->setId($item->getId())->save();
    }
}
