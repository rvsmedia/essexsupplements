<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Preorder
 * @author    Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\Preorder\Setup;

use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Setup\EavSetupFactory;

/**
 * Upgrade Data script
 * @codeCoverageIgnore
 */
class UpgradeData implements UpgradeDataInterface
{
    /**
     * EAV setup factory
     *
     * @var EavSetupFactory
     */
    private $eavSetupFactory;

    private $customerSetupFactory;

    private $_attributeSetFactory;

    /**
     * @param \Magento\Framework\Filesystem\Io\File $filesystemFile
     * @param EavSetupFactory                       $eavSetupFactory
     */
    public function __construct(
        EavSetupFactory $eavSetupFactory,
        \Magento\Customer\Setup\CustomerSetupFactory $customerSetupFactory,
        \Magento\Eav\Model\Entity\Attribute\SetFactory $attributeSetFactory
    ) {
        $this->eavSetupFactory = $eavSetupFactory;
        $this->customerSetupFactory = $customerSetupFactory;
        $this->_attributeSetFactory = $attributeSetFactory;
    }

    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        /** @var EavSetup $eavSetup */
        $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'wk_preorder_qty',
            [
                'type' => 'varchar',
                'backend' => '',
                'frontend' => '',
                'label' => 'Maximum Preorder Quantity',
                'input' => 'text',
                'class' => '',
                'source' => '',
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
                'visible' => true,
                'required' => false,
                'user_defined' => true,
                'default' => '',
                'searchable' => false,
                'filterable' => false,
                'comparable' => false,
                'visible_on_front' => false,
                'used_in_product_listing' => true,
                'unique' => false,
                'apply_to'     => 'simple,virtual',
                'frontend_class'=>'validate-number validate-greater-than-zero',
                'group' => 'Product Details'
            ]
        );

        /* Create Customer Preorder Fee Attribute*/

        /** @var CustomerSetup $customerSetup */
        $customerSetup = $this->customerSetupFactory->create(['setup' => $setup]);
        
        $customerEntity = $customerSetup->getEavConfig()->getEntityType('customer');
        $attributeSetId = $customerEntity->getDefaultAttributeSetId();
        
        /** @var $attributeSet AttributeSet */
        $attributeSet = $this->_attributeSetFactory->create();
        $attributeGroupId = $attributeSet->getDefaultGroupId($attributeSetId);

        $customerSetup->addAttribute(
            \Magento\Customer\Model\Customer::ENTITY,
            'preorder_fee',
            [
                'type'              => 'varchar',
                'label'             => 'Preorder Fee',
                'input'             => 'text',
                'frontend_class'    => 'validate-zero-or-greater',
                'backend_class'     => 'validate-zero-or-greater',
                'system'            => false,
                'required'          => false,
                'user_defined'      => true,
                'default'           => 0,
                'sort_order'        => 100,
                'position'          => 100,
                'visible'           => true
            ]
        );

        $attribute = $customerSetup->getEavConfig()
        ->getAttribute(
            \Magento\Customer\Model\Customer::ENTITY,
            'preorder_fee'
        )
        ->addData(
            [
                'attribute_set_id' => $attributeSetId,
                'attribute_group_id' => $attributeGroupId,
                'used_in_forms' => ['adminhtml_customer','customer_account_edit','customer_account_create'],
            ]
        );
        $attribute->save();

        $customerSetup = $this->customerSetupFactory->create(['setup' => $setup]);
        $customerSetup->addAttribute(
            \Magento\Customer\Model\Customer::ENTITY,
            'custom_preorder_fee',
            [
                'type' => 'int',
                'label' => 'Allow Custom Preorder Fee',
                'input' => 'select',
                'source' => 'Magento\Eav\Model\Entity\Attribute\Source\Boolean',
                'required' => false,
                'default' => '0',
                'sort_order' => 99,
                'system' => false,
                'position' => 99
            ]
        );
        $sampleAttribute = $customerSetup->getEavConfig()->getAttribute(\Magento\Customer\Model\Customer::ENTITY, 'custom_preorder_fee');
        $sampleAttribute->addData(
            [
                'attribute_set_id' => $attributeSetId,
                'attribute_group_id' => $attributeGroupId,
                'used_in_forms' => ['adminhtml_customer_address', 'customer_address_edit', 'customer_register_address', 'adminhtml_customer'],
            ]
        );
        $sampleAttribute->save();

        $customerSetup = $this->customerSetupFactory->create(['setup' => $setup]);
        $customerSetup->addAttribute(
            \Magento\Customer\Model\Customer::ENTITY,
            'preorder_fee_type',
            [
                'type' => 'text',
                'label' => 'Preorder Fee Calculation Method',
                'input' => 'select',
                'source' => 'Webkul\Preorder\Model\Entity\Attribute\Source\FeeType',
                'required' => false,
                'default' => 'F',
                'sort_order' => 101,
                'system' => false,
                'position' => 101
            ]
        );
        $sampleAttribute = $customerSetup->getEavConfig()->getAttribute(\Magento\Customer\Model\Customer::ENTITY, 'preorder_fee_type');
        $sampleAttribute->addData(
            [
                'attribute_set_id' => $attributeSetId,
                'attribute_group_id' => $attributeGroupId,
                'used_in_forms' => ['adminhtml_customer_address', 'customer_address_edit', 'customer_register_address', 'adminhtml_customer'],
            ]
        );
        $sampleAttribute->save();

        $setup->endSetup();
    }
}
