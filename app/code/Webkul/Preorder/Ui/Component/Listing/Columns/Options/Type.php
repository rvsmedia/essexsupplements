<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Preorder
 * @author    Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\Preorder\Ui\Component\Listing\Columns\Options;

use Magento\Framework\Data\OptionSourceInterface;
use Webkul\Preorder\Helper\Data as PreorderHelper;

class Type implements OptionSourceInterface
{
    /**
     * Get options
     *
     * @return array
     */
    public function toOptionArray()
    {
        $options = [
                        [
                            'label' => PreorderHelper::PREORDER_TYPE_COMPLETE_LABEL,
                            'value' => PreorderHelper::PREORDER_TYPE_COMPLETE
                        ],
                        [
                            'label' => PreorderHelper::PREORDER_TYPE_PARTIAL_LABEL,
                            'value' => PreorderHelper::PREORDER_TYPE_PARTIAL
                        ]
                    ];
        return $options;
    }
}
