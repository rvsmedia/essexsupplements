<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Preorder
 * @author    Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\Preorder\Ui\Component\Listing\Columns\Options;

use Magento\Framework\Data\OptionSourceInterface;
use Webkul\Preorder\Helper\Data as PreorderHelper;

class Status implements OptionSourceInterface
{
    /**
     * Get options
     *
     * @return array
     */
    public function toOptionArray()
    {
        $options = [
                        [
                            'label' => PreorderHelper::PREORDER_STATUS_PENDING_LABEL,
                            'value' => PreorderHelper::PREORDER_STATUS_PENDING
                        ],
                        [
                            'label' => PreorderHelper::PREORDER_STATUS_PROCESSING_LABEL,
                            'value' => PreorderHelper::PREORDER_STATUS_PROCESSING
                        ],
                        [
                            'label' => PreorderHelper::PREORDER_STATUS_COMPLETE_LABEL,
                            'value' => PreorderHelper::PREORDER_STATUS_COMPLETE
                        ]
                    ];
        return $options;
    }
}
