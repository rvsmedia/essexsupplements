<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Preorder
 * @author    Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\Preorder\Ui\Component\Listing\Columns;

use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;
use Magento\Framework\UrlInterface;
use Magento\Framework\Api\SearchCriteriaBuilder;

class PreorderAction extends Column
{
    /**
     * @var UrlInterface
     */
    protected $_urlBuilder;
	protected $_orderRepository;
    protected $_searchCriteria;

    /**
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param UrlInterface $urlBuilder
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        UrlInterface $urlBuilder,
		OrderRepositoryInterface $orderRepository,
        SearchCriteriaBuilder $criteria,
        array $components = [],
        array $data = []
    ) {
        $this->_urlBuilder = $urlBuilder;
		$this->_orderRepository = $orderRepository;
        $this->_searchCriteria  = $criteria;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     * @return array
     */
    
    public function prepareDataSource(array $dataSource)
    {
		
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as &$items) {
                $productArr = [];
                $order  = $this->_orderRepository->get($items["entity_id"]);

                foreach ($order->getAllVisibleItems() as $item) {
                    $productArr[] = $item->getSku();
                    //$productArr[] = $item->getName(); //Get product name
                }
                $items['products'] = implode(' - ', $productArr);
                unset($productArr);
            }
        }
        return $dataSource;
    }
	
	 public function prepareDdataSource(array $dataSource)
    {
		
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as &$items) {
                $productArr = [];
                $order  = $this->_orderRepository->get($items["entity_id"]);

                foreach ($order->getAllVisibleItems() as $item) {
                    $productArr[] = $item->getSku();
                    //$productArr[] = $item->getName(); //Get product name
                }
                $items['products'] = implode(' - ', $productArr);
                unset($productArr);
            }
        }
        return $dataSource;
    }
	
}
