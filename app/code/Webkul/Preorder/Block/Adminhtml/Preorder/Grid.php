<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Preorder
 * @author    Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\Preorder\Block\Adminhtml\Preorder;

use Webkul\Preorder\Model\ResourceModel\Preorder\CollectionFactory;
use Magento\Sales\Model\ResourceModel\Order\CollectionFactory as Orders;

class Grid extends \Magento\Backend\Block\Widget\Grid\Extended
{
    /**
     * @var \Magento\Framework\App\ResourceConnection
     */
    protected $_resource;

    /**
     * @var CollectionFactory
     */
    protected $_preorderCollection;

    /**
     * @var Orders
     */
    protected $_orderCollection;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Backend\Helper\Data $backendHelper
     * @param \Magento\Framework\App\ResourceConnection $resource
     * @param CollectionFactory $preorderCollection
     * @param Orders $orderCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
        \Magento\Framework\App\ResourceConnection $resource,
        CollectionFactory $preorderCollection,
        Orders $orderCollection,
        array $data = []
    ) {
        $this->_resource = $resource;
        $this->_preorderCollection = $preorderCollection;
        $this->_orderCollection = $orderCollection;
        parent::__construct($context, $backendHelper, $data);
    }

    /**
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('preorderPreorderGrid');
        $this->setDefaultSort('id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
        $this->setVarNameFilter('preorder_filter');
    }

    /**
     * @return $this
     */
    protected function _prepareCollection()
    {
        $orderIds = [];
        $collection = $this->_preorderCollection
                            ->create()
                            ->addFieldToSelect('order_id');
        foreach ($collection as $item) {
            $orderIds[] = $item->getOrderId();
        }

        $collection = $this->_orderCollection
                        ->create()
                        ->addAttributeToSelect('*')
                        ->addFieldToFilter('entity_id', ['in' => $orderIds]);
        $joinTable = $this->_resource->getTableName('sales_order_grid');
        $sql = 'main_table.entity_id = sog.entity_id';
        $collection->getSelect()->join($joinTable.' as sog', $sql, ['*']);
        $collection->addFilterToMap('customer_email', 'sog.customer_email');
        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    /**
     * @return $this
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    protected function _prepareColumns()
    {
        $this->addColumn(
            'increment_id',
            [
                'header' => __('Order Ref'),
                'type' => 'text',
                'index' => 'increment_id',
                'header_css_class' => 'col-id',
                'column_css_class' => 'col-id',
            ]
        );
        $this->addColumn(
            'billing_name',
            [
                'header' => __('Bill-to-Name'),
                'type' => 'text',
                'index' => 'billing_name',
            ]
        );
        $this->addColumn(
            'customer_email',
            [
                'header' => __('Customer Email'),
                'type' => 'text',
                'index' => 'customer_email',
            ]
        );
        $this->addColumn(
            'base_grand_total',
            [
                'header' => __('Grand Total (Base)'),
                'type' => 'price',
                'index' => 'base_grand_total',
                'type' => 'currency',
                'currency' => 'base_currency_code',
            ]
        );
        $this->addColumn(
            'grand_total',
            [
                'header' => __('Grand Total (Purchased)'),
                'type' => 'price',
                'index' => 'grand_total',
                'type' => 'currency',
                'currency' => 'order_currency_code',
            ]
        );
        $this->addColumn(
            'created_at',
            [
                'header' => __('Purchased Date'),
                'type' => 'date',
                'index' => 'created_at',
                'filter' => false,
            ]
        );

        return parent::_prepareColumns();
    }

    /**
     * After load collection.
     */
    protected function _afterLoadCollection()
    {
        $this->getCollection()->walk('afterLoad');
        parent::_afterLoadCollection();
    }

    /**
     * @return string
     */
    public function getGridUrl()
    {
        return $this->getUrl('preorder/preorder/grid', ['_current' => true]);
    }

    /**
     * @param \Magento\Framework\DataObject $row
     *
     * @return string
     */
    public function getRowUrl($row)
    {
        $store = $this->getRequest()->getParam('store');
        return $this->getUrl(
            'sales/order/view',
            ['store' => $store, 'order_id' => $row->getEntityId()]
        );
    }

    /**
     * @return $this
     */
    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('id');
        $this->getMassactionBlock()->setFormFieldName('preorder');
        $this->getMassactionBlock()->addItem(
            'email',
            [
                'label' => __('Send Email'),
                'url' => $this->getUrl('preorder/*/email'),
            ]
        );

        return $this;
    }
}
