<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Preorder
 * @author    Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\Preorder\Helper;

use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\App\Area;
use Magento\Framework\Pricing\PriceCurrencyInterface;
use Magento\Framework\Mail\Template\TransportBuilder;
use Magento\Framework\Translate\Inline\StateInterface;
use Magento\ConfigurableProduct\Model\Product\Type\Configurable;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory as Products;
use Webkul\Preorder\Model\ResourceModel\Item\CollectionFactory as Items;
use Webkul\Preorder\Model\ResourceModel\Preorder\CollectionFactory;
use Webkul\Preorder\Model\ResourceModel\Complete\CollectionFactory as CompleteCollection;
use Magento\Sales\Model\ResourceModel\Order\CollectionFactory as OrderCollection;
use Magento\Sales\Model\ResourceModel\Order\Invoice\Item\CollectionFactory as InvoiceItemCollection;
use Magento\CatalogInventory\Model\Spi\StockRegistryProviderInterface;
use Magento\CatalogInventory\Api\StockConfigurationInterface;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Psr\Log\LoggerInterface;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    const PREORDER_TYPE_COMPLETE = 0;
    const PREORDER_TYPE_PARTIAL = 1;
    const PREORDER_TYPE_COMPLETE_LABEL = "Complete Payment";
    const PREORDER_TYPE_PARTIAL_LABEL = "Partial Payment";
    const PREORDER_STATUS_PENDING = 0;
    const PREORDER_STATUS_PROCESSING = 1;
    const PREORDER_STATUS_COMPLETE = 2;
    const PREORDER_STATUS_PENDING_LABEL = "Pending";
    const PREORDER_STATUS_PROCESSING_LABEL = "Processing";
    const PREORDER_STATUS_COMPLETE_LABEL = "Complete";
    const CONFIG_PATH_NOTIFICATION_EMAIL = 'preorder/settings/preorder_notification_template';

    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    protected $_request;

    /**
     * @var \Magento\Framework\UrlInterface
     */
    protected $_urlBuilder;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $_scopeConfig;

    /**
     * @var \Magento\Framework\App\ResourceConnection
     */
    protected $_resource;

    /**
     * @var \Magento\Framework\Filesystem
     */
    protected $_filesystem;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @var TransportBuilder
     */
    protected $_transportBuilder;

    /**
     * @var StateInterface
     */
    protected $_inlineTranslation;

    /**
     * @var Configurable
     */
    protected $_configurable;

    /**
     * @var \Magento\Catalog\Model\ProductFactor
     */
    protected $_product;

    /**
     * @var \Magento\Sales\Model\OrderFactory
     */
    protected $_order;

    /**
     * @var \Magento\Catalog\Model\Product\OptionFactory
     */
    protected $_option;

    /**
     * @var Products
     */
    protected $_productCollection;

    /**
     * @var Items
     */
    protected $_itemCollection;

    /**
     * @var CollectionFactory
     */
    protected $_preorderCollection;

    /**
     * @var ProductFactory
     */
    protected $_catalogProduct;

    /**
     * @var CustomerRepositoryInterface
     */
    protected $_customerRepository;

    /**
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Framework\App\ResourceConnection $resource
     * @param \Magento\Framework\Filesystem $filesystem
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\Session\SessionManager $session
     * @param \Magento\Catalog\Model\ProductFactory $product
     * @param \Magento\Sales\Model\OrderFactory $order
     * @param \Magento\Catalog\Model\Product\OptionFactory $option
     * @param \Magento\Catalog\Model\Product $catalog
     * @param \Magento\Customer\Model\Session $customerSession
     * @param \Magento\Checkout\Model\CartFactory $cart
     * @param \Webkul\Preorder\Model\ItemFactory $item
     * @param \Webkul\Preorder\Model\ProductFactory $preorderProductFactory
     * @param TransportBuilder $transportBuilder
     * @param StateInterface $inlineTranslation
     * @param Configurable $configurable
     * @param Products $productCollection
     * @param Items $itemCollection
     * @param CollectionFactory $preorderCollection
     * @param CompleteCollection $completeCollection
     * @param OrderCollection $orderCollectionFactory
     * @param PriceCurrencyInterface $priceCurrency
     * @param \Magento\Framework\App\State $state
     * @param \Magento\Catalog\Helper\Product\Configuration $configuration
     * @param \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry
     * @param InvoiceItemCollection $invoiceItemCollection
     * @param StockRegistryProviderInterface $stockRegistryProvider
     * @param StockConfigurationInterface $stockConfiguration
     * @param \Magento\Directory\Model\CurrencyFactory $currencyFactory
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Framework\App\ResourceConnection $resource,
        \Magento\Framework\Filesystem $filesystem,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Session\SessionManager $session,
        \Magento\Catalog\Model\ProductFactory $product,
        \Magento\Sales\Model\OrderFactory $order,
        \Magento\Catalog\Model\Product\OptionFactory $option,
        \Magento\Catalog\Model\Product $catalog,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Checkout\Model\CartFactory $cart,
        \Webkul\Preorder\Model\ItemFactory $item,
        \Webkul\Preorder\Model\ProductFactory $preorderProductFactory,
        TransportBuilder $transportBuilder,
        StateInterface $inlineTranslation,
        Configurable $configurable,
        Products $productCollection,
        Items $itemCollection,
        CollectionFactory $preorderCollection,
        CompleteCollection $completeCollection,
        OrderCollection $orderCollectionFactory,
        PriceCurrencyInterface $priceCurrency,
        \Magento\Framework\App\State $state,
        \Magento\Catalog\Helper\Product\Configuration $configuration,
        \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry,
        InvoiceItemCollection $invoiceItemCollection,
        StockRegistryProviderInterface $stockRegistryProvider,
        StockConfigurationInterface $stockConfiguration,
        \Magento\Directory\Model\CurrencyFactory $currencyFactory,
        \Magento\Framework\Escaper $escaper,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        CustomerRepositoryInterface $customerRepository,
        ProductRepositoryInterface $productRepository,
        LoggerInterface $logger
    ) {
        $this->_request = $context->getRequest();
        $this->_urlBuilder = $context->getUrlBuilder();
        $this->_scopeConfig = $context->getScopeConfig();
        $this->_resource = $resource;
        $this->_filesystem = $filesystem;
        $this->_storeManager = $storeManager;
        $this->_session = $session;
        $this->_product = $product;
        $this->_order = $order;
        $this->_option = $option;
        $this->_catalogProduct = $catalog;
        $this->_customerSession = $customerSession;
        $this->_cart = $cart;
        $this->_item = $item;
        $this->_preorderProduct = $preorderProductFactory;
        $this->_transportBuilder = $transportBuilder;
        $this->_inlineTranslation = $inlineTranslation;
        $this->_configurable = $configurable;
        $this->_productCollection = $productCollection;
        $this->_completeCollection = $completeCollection;
        $this->_itemCollection = $itemCollection;
        $this->_preorderCollection = $preorderCollection;
        $this->_orderCollectionFactory = $orderCollectionFactory;
        $this->_priceCurrency = $priceCurrency;
        $this->_state = $state;
        $this->_configuration = $configuration;
        $this->_stockRegistry = $stockRegistry;
        $this->_invoiceItemCollection = $invoiceItemCollection;
        $this->_stockRegistryProvider = $stockRegistryProvider;
        $this->_stockConfiguration = $stockConfiguration;
        $this->_currencyFactory = $currencyFactory;
        $this->_escaper = $escaper;
        $this->_objectManager = $objectManager;
        $this->_customerRepository = $customerRepository;
        $this->productRepository = $productRepository;
        $this->logger = $logger;
        parent::__construct($context);
    }

    /**
     * Get Preorder Type.
     *
     * @return int
     */
    public function getPreorderType()
    {
        $config = 'preorder/settings/preorder_type';
        $type = (int) $this->_scopeConfig->getValue(
            $config,
            \Magento\Store\Model\ScopeInterface::SCOPE_WEBSITES
        );
        if ($this->getPreorderPercent() == 100) {
            $type = self::PREORDER_TYPE_COMPLETE;
        }

        return $type;
    }

    /**
     * Get Preorder Percent.
     *
     * @return int
     */
    public function getPreorderPercent()
    {
        $config = 'preorder/settings/preorder_percent';
        $preorderPercent = (float) $this->_scopeConfig->getValue(
            $config,
            \Magento\Store\Model\ScopeInterface::SCOPE_WEBSITES
        );
        if ($preorderPercent < 0) {
            $preorderPercent = 0;
        }

        if ($preorderPercent > 100) {
            $preorderPercent = 100;
        }

        $config = 'preorder/settings/preorder_type';
        $type = (int) $this->_scopeConfig->getValue(
            $config,
            \Magento\Store\Model\ScopeInterface::SCOPE_WEBSITES
        );
        if ($type == self::PREORDER_TYPE_COMPLETE) {
            $preorderPercent = 100;
        }

        return $preorderPercent;
    }

    /**
     * Get Admin Email Id.
     *
     * @return string
     */
    public function getAdminEmail()
    {
        $config = 'preorder/settings/preorder_email';
        return $this->_scopeConfig->getValue(
            $config,
            \Magento\Store\Model\ScopeInterface::SCOPE_WEBSITES
        );
    }

    /**
     * Get Auto Email Configuration.
     *
     * @return bool
     */
    public function getAutoEmail()
    {
        $config = 'preorder/settings/preorder_email_type';
        return $this->_scopeConfig->getValue(
            $config,
            \Magento\Store\Model\ScopeInterface::SCOPE_WEBSITES
        );
    }

    /**
     * Get Admin Email Id.
     *
     * @return string
     */
    public function getOrderNote()
    {
        $config = 'preorder/settings/preorder_order_note';
        $msg = $this->_scopeConfig->getValue(
            $config,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
        $msg = $this->_escaper->escapeHtml($msg);
        return $msg;
    }

    /**
     * Get Stock Status of Product.
     *
     * @param int $productId
     *
     * @return bool
     */
    public function getStockStatus($productId)
    {
        $stockDetails = $this->getStockDetails($productId);

        return $stockDetails['is_in_stock'];
    }

    /**
     * Check Product is Preorder or Not.
     *
     * @param int  $productId
     * @param bool $stockStatus [optional]
     *
     * @return bool
     */
    public function isPreorder($productId, $stockStatus = '')
    {
        $isProduct = false;
        $productId = (int) $productId;
        if (!$this->isValidProductId($productId)) {
            return false;
        }
        $collection = $this->_productCollection->create();
        $collection->addFieldToFilter('entity_id', $productId);
        $collection->addAttributeToSelect('*');

        foreach ($collection as $item) {
            $product = $item;
            $isProduct = true;
            break;
        }

        if (!$isProduct) {
            return false;
        }
        $productType = $product->getTypeId();
        $productTypeArray = ['configurable', 'bundle', 'grouped', 'downloadable'];
        $allowedProductType = ['simple', 'virtual'];
        if (!in_array($productType, $allowedProductType)) {
            return false;
        }

        if ($stockStatus == '') {
            $stockStatus = $this->getStockStatus($productId);
        }
        if (!$stockStatus) {
            $pendingQty = 0;
            $preorderStatus = $product->getWkPreorder();
            if ($preorderStatus) {
                $itemCollection = $this->_preorderCollection->create();
                $itemCollection->addFieldToFilter('product_id', $productId)
                                ->addFieldToFilter('preorder_status', ['neq'=> 2]);
                $pendingOrderCount = $itemCollection->getSize();
                if ($pendingOrderCount) {
                    foreach ($itemCollection as $key) {
                        $pendingQty += $key->getQty();
                    }
                }
                $maxPreorderQty = $product->getWkPreorderQty();
                
                if (($maxPreorderQty - $pendingQty > 0) || !$maxPreorderQty) {
                    return $preorderStatus;
                }
            }
        }
        return false;
    }

    /**
     * Check Whether Product Id is Valid or Not
     *
     * @param int $productId
     *
     * @return bool
     */
    public function isValidProductId($productId)
    {
        $preorderCompleteProductId = $this->getPreorderCompleteProductId();
        if ($productId == $preorderCompleteProductId) {
            return false;
        }

        if ($productId == '' || $productId == 0) {
            return false;
        }

        return true;
    }
    /**
     * Check Configurable Product is Preorder or Not.
     *
     * @param int $productId
     *
     * @return bool
     */
    public function isConfigPreorder($productId, $stockStatus = '')
    {
        $isProduct = false;
        $collection = $this->_productCollection->create();
        $collection->addFieldToFilter('entity_id', $productId);
        $collection->addAttributeToSelect('*');
        foreach ($collection as $item) {
            $product = $item;
            $isProduct = true;
            break;
        }
        if ($isProduct) {
            $stock = $this->getStockDetails($productId);
            if (!$stock['is_in_stock']) {
                return false;
            }
            $productType = $product->getTypeId();
            if ($productType == 'configurable') {
                $configModel = $this->_configurable;
                $usedProductIds = $configModel->getUsedProductIds($product);
                foreach ($usedProductIds as $usedProductId) {
                    if ($stockStatus != '') {
                        if ($this->isPreorder($usedProductId, $stockStatus)) {
                            return true;
                        }
                    } else {
                        if ($this->isPreorder($usedProductId)) {
                            return true;
                        }
                    }
                }
            }
        }

        return false;
    }

    /**
     * Get Mediad Path.
     *
     * @return string
     */
    public function getMediaPath()
    {
        return $this->_filesystem
                    ->getDirectoryRead(DirectoryList::MEDIA)
                    ->getAbsolutePath();
    }
    /**
     * Check Product is Partial Preorder or Not.
     *
     * @param int $productId
     *
     * @return bool
     */
    public function isPartialPreorder($productId)
    {
        if (!$this->isPreorder($productId)) {
            return false;
        }

        $preorderType = $this->getPreorderType();
        if ($preorderType == self::PREORDER_TYPE_PARTIAL) {
            return true;
        }

        return false;
    }

    /**
     * Get Product's Price.
     *
     * @param int $productId
     *
     * @return float
     */
    public function getPrice($product)
    {
        $price = $product->getFinalPrice();

        return $price;
    }

    /**
     * Get Partial Preorder Price.
     *
     * @param object $product
     * @param int    $productId
     *
     * @return flaot
     */
    public function getPreorderPrice($product, $productId)
    {
        $price = $this->getPrice($product, $productId);
        if ($this->isPartialPreorder($productId)) {
            $preorderPercent = $this->getPreorderPercent();
            if ($preorderPercent > 0) {
                $price = ($price * $preorderPercent) / 100;
            }
        }

        return $price;
    }

    /**
     * Get Order by Id.
     *
     * @param int $orderId
     *
     * @return object
     */
    public function getOrder($orderId)
    {
        return $this->_order->create()->load($orderId);
    }

    /**
     * Check Order Item is Preorder or Not.
     *
     * @param int $itemId
     * @param bool $useOrder
     *
     * @return bool
     */
    public function isPreorderOrderedItem($itemId, $useOrder = false)
    {
        if ($useOrder) {
            $collection = $this->_preorderCollection->create();
            $item = $this->getDataByField($itemId, 'item_id', $collection);
        } else {
            $collection = $this->_itemCollection->create();
            $item = $this->getDataByField([$itemId, 1], ['item_id', 'status'], $collection);
        }

        if ($item) {
            return true;
        }

        return false;
    }

    /**
     * Get Preorder Status.
     *
     * @param int $itemId
     *
     * @return int
     */
    public function getPreorderStatus($itemId)
    {
        $status = 0;
        $collection = $this->_preorderCollection->create();
        $item = $this->getDataByField($itemId, 'item_id', $collection);
        if ($item) {
            $status = $item->getPreorderStatus();
        }

        return $status;
    }

    /**
     * Check Product is Child Product or Not.
     *
     * @return bool
     */
    public function isChildProduct()
    {
        $productId = $this->_request->getParam('id');
        $productModel = $this->_product->create();
        $product = $productModel->load($productId);
        $productType = $product->getTypeID();
        $productTypeArray = ['bundle', 'grouped'];
        if (in_array($productType, $productTypeArray)) {
            return true;
        }

        return false;
    }

    /**
     * Check Preorder Option is Allowed for Product Type.
     *
     * @return bool
     */
    public function showPreorderOption()
    {
        $productId = $this->_request->getParam('id');
        $productModel = $this->_product->create();
        $product = $productModel->load($productId);
        $productType = $product->getTypeID();
        $productTypeArray = ['configurable', 'bundle', 'grouped', 'downloadable'];
        if (in_array($productType, $productTypeArray)) {
            return 0;
        }

        return 1;
    }

    /**
     * Get Url To Check Configurable Product is Preorder or Not.
     *
     * @return string
     */
    public function getCheckConfigUrl()
    {
        return $this->_urlBuilder->getUrl('preorder/preorder/check/');
    }

    /**
     * Get Url To Complete Preorder Order.
     *
     * @return string
     */
    public function getCompletePreorderUrl()
    {
        return $this->_urlBuilder->getUrl('preorder/preorder/complete/');
    }

    /**
     * Get Account Login Url For Customer.
     *
     * @return string
     */
    public function getLogInUrl()
    {
        return $this->_urlBuilder->getUrl('customer/account/login/');
    }

    /**
     * Get Product by Id.
     *
     * @param int $productId
     *
     * @return object
     */
    public function getProduct($productId)
    {
        return $this->_product->create()->load($productId);
    }

    /**
     * Get Prorder Complete Product Id.
     *
     * @return int
     */
    public function getPreorderCompleteProductId()
    {
        $productModel = $this->_product->create();
        $productId = (int) $productModel->getIdBySku('preorder_complete');

        return $productId;
    }

    /**
     * Get Preorder Complete Product's Options.
     *
     * @return json
     */
    public function getPreorderCompleteOptions($json = true)
    {
        $array = [];
        $productId = (int) $this->getPreorderCompleteProductId();
        $product = $this->_product->create()->load($productId);
        foreach ($product->getOptions() as $option) {
            $optionId = $option->getId();
            $optionTitle = $option->getTitle();
            $array[] = ['id' => $optionId, 'title' => $optionTitle];
        }

        if ($json) {
            return json_encode($array);
        }

        return $array;
    }

    /**
     * Get Html Block of Pay Preorder Amount (Partial Preorder).
     *
     * @return html
     */
    public function getPayPreOrderHtml($isListPage = false)
    {
        $html = '';
        $class = "wk-msg-box wk-info wk-pay-preorer-amount";
        if ($isListPage) {
            $class = "wk-msg-box wk-cat-msg-box wk-info wk-pay-preorer-amount";
        }

        $type = $this->getPreorderType();
        $percent = $this->getPreorderPercent();
        if ($type == self::PREORDER_TYPE_PARTIAL) {
            $html .= "<div class='".$class."'>";
            if ($percent > 0) {
                $html .= __('Pay %1% as Preorder.', $percent);
            } else {
                $msg = $this->_scopeConfig->getValue(
                    'preorder/settings/preorder_free_note',
                    \Magento\Store\Model\ScopeInterface::SCOPE_STORE
                );
                $msg = $this->_escaper->escapeHtml($msg);
                $html .= $msg;
            }

            $html .= '</div>';
        }

        return $html;
    }

    /**
     * Get Stock Details of Product.
     *
     * @param int $productId
     *
     * @return array
     */
    public function getStockDetails($productId)
    {
        $connection = $this->_resource->getConnection();
        $stockDetails = ['is_in_stock' => 0, 'qty' => 0];
        $collection = $this->_productCollection
                            ->create()
                            ->addAttributeToSelect('name');
        $table = $connection->getTableName('cataloginventory_stock_item');
        $bind = 'product_id = entity_id';
        $cond = '{{table}}.stock_id = 1';
        $type = 'left';
        $alias = 'is_in_stock';
        $field = 'is_in_stock';
        $collection->joinField($alias, $table, $field, $bind, $cond, $type);
        $alias = 'qty';
        $field = 'qty';
        $collection->joinField($alias, $table, $field, $bind, $cond, $type);
        $collection->addFieldToFilter('entity_id', $productId);
        foreach ($collection as $value) {
            $stockDetails['qty'] = $value->getQty();
            $stockDetails['is_in_stock'] = $value->getIsInStock();
            $stockDetails['name'] = $value->getName();
        }

        return $stockDetails;
    }

    /**
     * Check Product is Available or Not to Complete Preorder.
     *
     * @param int $productId
     * @param int $qty
     * @param int $isQty
     *
     * @return bool
     */
    public function isAvailable($productId, $qty, $isQty = 0)
    {
        $stockDetails = $this->getStockDetails($productId);
        if ($stockDetails['is_in_stock'] == 1) {
            if ($isQty == 0) {
                if ($stockDetails['qty'] >= $qty) {
                    return true;
                }
            } else {
                return true;
            }
        }

        return false;
    }

    /**
     * Check if Configurable Product is Available or Not to Complete Preorder.
     *
     * @param int $productId
     * @param int $qty
     * @param int $parentId
     *
     * @return bool
     */
    public function isConfigAvailable($productId, $qty, $parentId)
    {
        if ($this->isAvailable($productId, $qty)) {
            if ($this->isAvailable($parentId, $qty, 1)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Get Html Block of Preorder Info Block.
     *
     * @param int $productId
     *
     * @return html
     */
    public function getPreOrderInfoBlock($productId)
    {
        $html = '';
        $displayDate = false;
        $today = date('m/d/y');
        $product = $this->getProduct($productId);
        $availability = $product->getPreorderAvailability();
        if ($availability != '') {
            $date = date_create($availability);
            $dispDate = date_format($date, 'l jS F Y');
            $date = date_format($date, 'm/d/y');
            if (strtotime($date) > strtotime($today)) {
                $displayDate = true;
            }
        }

        $msg = $this->_scopeConfig->getValue(
            'preorder/settings/preorder_msg',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
        $msg = $this->_escaper->escapeHtml($msg);
        $msg = str_replace("\n", '<br>', $msg);
        if ($msg != '') {
            $html .= "<div class='wk-msg-box wk-info'>";
            $html .= $msg;
            $html .= '</div>';
        }

        if ($displayDate) {
            $html .= "<div class='wk-msg-box wk-info wk-availability-block'>";
            $html .= "<span class='wk-date-title'>";
            $html .= __('Available On');
            $html .= ' :</span>';
            $html .= "<span class='wk-date'>".$dispDate.'</span>';
            $html .= '</div>';
        }

        return $html;
    }

    /**
     * Get Customer Id's who Ordered Preorder Items.
     *
     * @param int $productId
     *
     * @return array
     */
    public function getCustomerIds($productId)
    {
        $emailIds = [];
        $collection = $this->_preorderCollection
                            ->create()
                            ->addFieldToFilter('product_id', $productId)
                            ->addFieldToSelect('customer_id');
        foreach ($collection as $value) {
            $emailIds[] = $value->getCustomerId();
        }

        $emailIds = array_unique($emailIds);

        return $emailIds;
    }

    /**
     * Get Email Id's of Customers who Ordered Preorder Items.
     *
     * @param int $productId
     *
     * @return array
     */
    public function getCustomerEmailIds($productId)
    {
        $emailIds = [];
        $collection = $this->_preorderCollection
                            ->create()
                            ->addFieldToFilter('product_id', $productId)
                            ->addFieldToFilter('preorder_status', 0)
                            ->addFieldToFilter('invoice_status', ['gteq' => 1])
                            ->addFieldToSelect('buyer_email');
        foreach ($collection as $value) {
            $emailIds[] = $value->getBuyerEmail();
        }

        $emailIds = array_unique($emailIds);

        return $emailIds;
    }

    /**
     * Create Preorder Complete Product if Not Exists.
     */
    public function createPreOrderProduct()
    {
        $preorderProductId = $this->getPreorderCompleteProductId();
        $attributeSetId = $this->_catalogProduct->getDefaultAttributeSetId();
        if ($preorderProductId == 0 || $preorderProductId == '') {
            try {
                $websiteIds = $this->getWebsiteIds();
                $stockData = [
                                'use_config_manage_stock' => 0,
                                'manage_stock' => 0,
                                'is_in_stock' => 1,
                                'qty' => 999999,
                            ];
                $preorderProduct = $this->_product->create();
                $preorderProduct->setSku('preorder_complete');
                $preorderProduct->setName('Complete PreOrder');
                $preorderProduct->setAttributeSetId($attributeSetId);
                $preorderProduct->setCategoryIds([2]);
                $preorderProduct->setWebsiteIds($websiteIds);
                $preorderProduct->setStatus(1);
                $preorderProduct->setVisibility(1);
                $preorderProduct->setTaxClassId(0);
                $preorderProduct->setTypeId('virtual');
                $preorderProduct->setPrice(0);
                $preorderProduct->setStockData($stockData);
                $preorderProduct->save();
                $this->addImage($preorderProduct);
                $this->setCustomOption($preorderProduct);
                $this->updateProduct($preorderProduct->getId());
            } catch (\Exception $e) {
                $e->getMessage();
            }
        }
    }

    /**
     * Add Image to Preorder Complete Product
     *
     * @param object $product
     */
    public function addImage($product)
    {
        $path = $this->getMediaPath().'preorder/images/preorder.png';
        if (file_exists($path)) {
            $types = ['image', 'small_image', 'thumbnail'];
            $product->addImageToMediaGallery($path, $types, false, false);
            $product->save();
        }
    }

    /**
     * Set Custom Option in Preorder Complete Product
     */
    public function setCustomOption($product)
    {
        $product->setHasOptions(1);
        $product->getResource()->save($product);
        $nameOption = [
                        'sort_order' => 1,
                        'title' => 'Product Name',
                        'price_type' => 'fixed',
                        'price' => 0,
                        'type' => 'field',
                        'is_require' => 1,
                    ];
        $orderOption = [
                        'sort_order' => 2,
                        'title' => 'Order Refernce',
                        'price_type' => 'fixed',
                        'price' => 0,
                        'type' => 'field',
                        'is_require' => 1,
                    ];
        $infoOption = [
                        'sort_order' => 3,
                        'title' => 'Options Info',
                        'price_type' => 'fixed',
                        'price' => 0,
                        'type' => 'area',
                        'is_require' => 0,
                    ];
        $options = [$nameOption, $orderOption, $infoOption];
        foreach ($options as $arrayOption) {
            $this->createCustomOption($product, $arrayOption);
        }
    }

    /**
     * Create Custom Option
     *
     * @param object $product
     * @param array $data
     */
    public function createCustomOption($product, $data)
    {
        try {
            $option = $this->_option
                            ->create()
                            ->setProductId($product->getId())
                            ->setStoreId($product->getStoreId())
                            ->addData($data);
            $option->save();
            $product->addOption($option);
        } catch (\Exception $e) {
            $error = $e->getMessage();
        }
    }

    /**
     * Create Options Info Custom Option
     */
    public function createOptionsInfoCustomOption()
    {
        $productId = $this->getPreorderCompleteProductId();
        if ($productId > 0) {
            $needOption = true;
            $options = $this->getPreorderCompleteOptions(false);
            foreach ($options as $option) {
                if ($option['title'] == 'Options Info') {
                    $needOption = false;
                    break;
                }
            }

            if ($needOption) {
                $infoOption = [
                        'sort_order' => 3,
                        'title' => 'Options Info',
                        'price_type' => 'fixed',
                        'price' => 0,
                        'type' => 'area',
                        'is_require' => 0,
                    ];

                $product = $this->getProduct($productId);
                $this->createCustomOption($product, $infoOption);
            }
        }
    }

    /**
     * Get All Website Ids.
     *
     * @return array
     */
    public function getWebsiteIds()
    {
        $websiteIds = [];
        $websites = $this->_storeManager->getWebsites();
        foreach ($websites as $website) {
            $websiteIds[] = $website->getId();
        }

        return $websiteIds;
    }

    /**
     * Get First Object From Collection
     *
     * @param array | int | string $value
     * @param array | string $field
     * @param object $collection
     *
     * @return object | bool
     */
    public function getDataByField($values, $fields, $collection)
    {
        $item = false;
        if (is_array($values)) {
            foreach ($values as $key => $value) {
                $field = $fields[$key];
                $collection = $collection->addFieldToFilter($field, $value);
            }
        } else {
            $collection = $collection->addFieldToFilter($fields, $values);
        }

        foreach ($collection as $item) {
            return $item;
        }

        return $item;
    }

    /**
     * Get Associated Product Id
     *
     * @param string $attribute
     * @param object $product
     *
     * @return int
     */
    public function getAssociatedId($attribute, $product)
    {
        $configModel = $this->_configurable;
        $product = $configModel->getProductByAttributes($attribute, $product);
        $productId = $product->getId();
        return $productId;
    }

    /**
     * Update Product Options
     *
     * @param int $productId
     */
    public function updateProduct($productId, $catalogProduct = false)
    {
        $updateRequired = true;
        if ($catalogProduct) {
            $updateRequired = false;
            if ($catalogProduct->getHasOptions()) {
                $data = [
                            'has_options' => $catalogProduct->getHasOptions(),
                            'required_options' => $catalogProduct->getRequiredOptions()
                        ];
                $updateRequired = true;
            }
        } else {
            $data = ['has_options' => 1, 'required_options' => 1];
        }

        if ($updateRequired) {
            $product = $this->_preorderProduct->create()->load($productId);
            $product->addData($data)->setId($productId)->save();
        }
    }

    /**
     * Check Whether Product Id Is Preorder Complete Product Id Or Not
     *
     * @param int $productId
     *
     * @return bool
     */
    public function isPreorderCompleteProduct($product, $isProduct = false)
    {
        if ($isProduct) {
            if ($product->getSku() == "preorder_complete") {
                return true;
            } else {
                return false;
            }
        }

        if ($product == $this->getPreorderCompleteProductId()) {
            return true;
        }

        return false;
    }

    /**
     * Save Cart
     */
    public function saveCart()
    {
        $cartModel = $this->_cart->create();
        $quote = $cartModel->getQuote();
        foreach ($quote->getAllItems() as $item) {
            $parentItem = ($item->getParentItem() ? $item->getParentItem() : $item);
            $parentItem->save();
        }

        $quote->save();
        $cartModel->save();
    }

    /**
     * Collect Totals
     *
     * @param object $quote
     */
    public function collectTotals($quote)
    {
        foreach ($quote->getAllItems() as $item) {
            $productId = $item->getProductId();
            if ($this->isPreorderCompleteProduct($productId)) {
                $this->setProductCompletePrice($item);
            } else {
                if ($this->isPreorder($productId)) {
                    $status = 1;
                    $this->setProductPrice($item);
                } else {
                    $status = 0;
                    $this->refreshProductPrice($item);
                }

                $this->updatePreorderData($item, $status);
            }
        }
    }

    /**
     * Check Customer is Logged In or Not
     *
     * @return bool
     */
    public function isLoggedIn()
    {
        if ($this->_customerSession->isLoggedIn()) {
            return true;
        }

        return false;
    }

    /**
     * Check Whether Guest is Logged In or Not
     *
     * @return bool
     */
    public function isGuestLoggedIn()
    {
        $email = trim($this->_session->getGuestEmailId());
        if ($email != "") {
            return true;
        }

        return false;
    }

    /**
     * Check Guest Details
     *
     * @param int $incrementId
     * @param string $emil
     *
     * @return bool
     */
    public function authenticate($incrementId, $email)
    {
        $orders = $this->_orderCollectionFactory->create()
                        ->addFieldToSelect('*')
                        ->addFieldToFilter('customer_email', $email)
                        ->addFieldToFilter('increment_id', $incrementId)
                        ->addFieldToFilter('customer_is_guest', 1)
                        ->setPageSize(1);
        if ($orders->getSize()) {
            return true;
        }

        return false;
    }

    /**
     * Login Guest
     *
     * @param string $email
     */
    public function loginGuest($email)
    {
        $this->_session->setGuestEmailId($email);
    }

    /**
     * Get Guest Email Id
     *
     * @return bool
     */
    public function getGuestEmailId()
    {
        return trim($this->_session->getGuestEmailId());
    }

    public function getOriginalPriceByItem($item)
    {
        $price = 0;
        $product = $item->getProduct();
        if ($item->getParentItem()) {
            $price = $item->getParentItem()
                        ->getProduct()
                        ->getPriceModel()
                        ->getChildFinalPrice(
                            $item->getParentItem()->getProduct(),
                            $item->getParentItem()->getQty(),
                            $product,
                            $item->getQty()
                        );
        } elseif (!$item->getParentItem()) {
            $price = $product->getFinalPrice($item->getQty());
        }

        return $price;
    }

    public function setProductPrice($item)
    {
        $product = $item->getProduct();
        $price = $this->getOriginalPriceByItem($item);
        $price = $this->getCurrentPrice($price);
        if ($this->isPartialPreorder($product->getId())) {
            $preorderPercent = $this->getPreorderPercent();
            if ($preorderPercent >= 0) {
                $price = ($price * $preorderPercent) / 100;
            }
        }

        if ($item->getParentItem()) {
            $parentItem = $item->getParentItem();
            $parentItem->setCustomPrice($price);
            $parentItem->setOriginalCustomPrice($price);
            $parentItem->setRowTotal($item->getQty()*$price);
            $parentItem->getProduct()->setIsSuperMode(true);
        } else {
            $item->setCustomPrice($price);
            $item->setOriginalCustomPrice($price);
            $item->setRowTotal($item->getQty()*$price);
            $item->getProduct()->setIsSuperMode(true);
        }
    }

    public function setProductCompletePrice($item)
    {
        $itemId = (int) $item->getId();
        $collection = $this->_completeCollection->create();
        $tableName = $this->_resource->getTableName('wk_preorder_items');
        $sql = "preorder.item_id = main_table.order_item_id ";
        $collection->getSelect()->join(['preorder' => $tableName], $sql, ['*']);
        $collection->getSelect()->where("main_table.quote_item_id = ".$itemId);
        if ($collection->getSize()) {
            foreach ($collection as $preorderItem) {
                $unitPrice = $this->getUnitPrice($preorderItem);
                $item->setCustomPrice($unitPrice);
                $item->setOriginalCustomPrice($unitPrice);
                $item->setRowTotal($item->getQty()*$unitPrice);
                $item->getProduct()->setIsSuperMode(true);
            }
        }
    }

    public function updatePreorderData($item, $status = 1)
    {
        $id = (int) $item->getId();
        if ($id > 0) {
            $preorderPercent = $this->getPreorderPercent();
            $collection = $this->_itemCollection->create();
            $item = $this->getDataByField($id, 'item_id', $collection);
            $data = [
                        'item_id' => $id,
                        'preorder_percent' => $preorderPercent,
                        'status' => $status
                    ];
            if ($item) {
                $this->_item->create()->addData($data)->setId($item->getId())->save();
            } else {
                $this->_item->create()->setData($data)->save();
            }
        }
    }

    /**
     * Refresh Product Price In Cart
     *
     * @param object $item
     */
    public function refreshProductPrice($item)
    {
        if ($this->allowRefreshItemPrice($item)) {
            $item->setCustomPrice(null);
            $item->setOriginalCustomPrice(null);
            $item->getProduct()->setIsSuperMode(true);
        }
    }

    /**
     * Get Price According To Current Store
     *
     * @param float $price
     *
     * @return float
     */
    public function getCurrentPrice($price)
    {
        $currency = null;
        $store = $this->_storeManager->getStore()->getStoreId();
        $price = $this->_priceCurrency->convert($price, $store, $currency);
        return $price;
    }

    /**
     * Get Price With Currency
     *
     * @param float $price
     *
     * @return string
     */
    public function getPriceWithCurrency($price)
    {
        $price = $this->_priceCurrency->convertAndFormat($price);
        return $price;
    }

    /**
     * Save Quote Item Updates
     *
     * @return string
     */
    public function checkStatus()
    {
        $this->_cart->create()->save();
        $cartModel = $this->_cart->create();
        $quote = $cartModel->getQuote();
        $this->collectTotals($quote);
    }

    /**
     * Get Cart
     *
     * @return object
     */
    public function getCart()
    {
        $cartModel = $this->_cart->create();
        return $cartModel;
    }

    /**
     * Check Whether it is Admin Area
     */
    public function isAdminArea()
    {
        if ($this->_state->getAreaCode() == "adminhtml") {
            return true;
        }

        return false;
    }

    /**
     * Get Order Note Html
     *
     * @param order $order
     *
     * @return string
     */
    public function getOrderNoteHtml($order)
    {
        $msgHtml = "";
        $isPreorderOrdredItem = false;
        if ($order == null) {
            return $msgHtml;
        }

        try {
            foreach ($order->getAllItems() as $item) {
                if ($this->isPreorderOrderedItem($item->getQuoteItemId())) {
                    $isPreorderOrdredItem = true;
                    break;
                }
            }
        } catch (\Exception $e) {
            $error = $e->getMessage();
        }

        if ($isPreorderOrdredItem) {
            $msgHtml = $this->getNoteHtml();
        }

        return $msgHtml;
    }

    /**
     * Get Note Html
     *
     * @return string
     */
    public function getNoteHtml()
    {
        $css = [];
        $css[] = "margin-top:25px";
        $css[] = "font-size:17px";
        $css[] = "font-weight:600";
        $css[] = "background-color: #d9edf7";
        $css[] = "border-color: #bce8f1";
        $css[] = "color: #31708f";
        $css[] = "border-radius: 4px";
        $css[] = "border: 1px solid transparent";
        $css[] = "padding: 10px";
        $css = implode("; ", $css);
        $msgHtml = '<div style="'.$css.'">';
        $msgHtml .= $this->getOrderNote();
        $msgHtml .= '</div>';

        return $msgHtml;
    }

    /**
     * Update Stock Data of Product
     *
     * @param int $productId
     * @param int $qty
     * @param bool $decrease
     */
    public function updateStockData($productId, $qty, $decrease = false)
    {
        try {
            $scopeId = $this->_stockConfiguration->getDefaultScopeId();
            $stockItem = $this->_stockRegistryProvider->getStockItem($productId, $scopeId);
            if ($stockItem->getItemId()) {
                if ($decrease) {
                    $qty = $stockItem->getQty(true) - $qty;
                } else {
                    $qty = $qty + $stockItem->getQty(true);
                }

                $stockItem->setQty($qty);
                $stockItem->save();
            }
        } catch (\Exception $e) {
            $e->getMessage();
        }
    }

    /**
     * Get Preorder Item Details
     *
     * @param int $itemId
     *
     * @return object
     */
    public function getPreorderItemDetails($itemId)
    {
        $item = false;
        $collection = $this->_preorderCollection
                            ->create()
                            ->addFieldToFilter('item_id', $itemId);
        if ($collection->getSize()) {
            foreach ($collection as $item) {
                return $item;
            }
        }

        return $item;
    }

    /**
     * Get Order View Url
     *
     * @param int $incrementId
     *
     * @return string
     */
    public function getOrderViewUrl($incrementId)
    {
        if ($this->isLoggedIn()) {
            $incrementId = str_replace("#", "", $incrementId);
            $order = $this->_order->create()->loadByIncrementId($incrementId);
            $orderId = $order->getId();
            return $this->_urlBuilder->getUrl('sales/order/view/')."order_id/$orderId";
        } else {
            return $this->_urlBuilder->getUrl('sales/guest/view/');
        }
    }

    /**
     * Get Parameters
     *
     * @return array
     */
    public function getParams()
    {
        return $this->_request->getParams();
    }

    /**
     * Get Order Items With Preorder Info
     *
     * @param object $order
     *
     * @return array
     */
    public function getOrderItemsResult($order)
    {
        $items = $order->getItemsCollection();
        $result = [];
        $info = [];
        $displayMessage = false;
        $count = 0;
        foreach ($items as $item) {
            $itemId = $item->getId();
            $productId = $item->getProductId();
            if ($item->getParentItem()) {
                $preorderItem = $this->getPreorderItemDetails($itemId);
                $parentId = $item->getParentItem()->getProductId();
                $key = $count-1;
                $info[$key]['item_id'] = $itemId;
                $info[$key]['product_id'] = $productId;
                $info[$key]['parent_id'] = $parentId;
                if ($this->isPreorderOrderedItem($item->getId(), true)) {
                    $displayMessage = true;
                    $info[$key]['preorder'] = $this->getPreorderStatus($itemId);
                    if ($this->isConfigAvailable($productId, $qty, $parentId) && $this->isPreorderItemInvoiced($item, $qty)) {
                        if ($info[$key]['preorder'] == self::PREORDER_STATUS_PENDING) {
                            $info[$key]['preorder_completion_allowed'] = true;
                        }
                    }
                    $info[$key]["preorder_option_html"] = $this->getOptionHtml($preorderItem);
                }
            } else {
                $extraInfo = $this->getOptionsInfo($item);
                $preorderItem = $this->getPreorderItemDetails($itemId);
                $qty = $item->getQtyOrdered();
                $name = $item->getName();
                $itemInfo = [
                                'preorder' => null,
                                'item_id' => $itemId,
                                'parent_id' => 0,
                                'product_id' => $productId,
                                'product_name' => $name,
                                'qty' => $qty,
                                'preorder_option_html' => '',
                                'preorder_completion_allowed' => false,
                                'options_info' => $extraInfo
                            ];
                if ($this->isPreorderOrderedItem($item->getId(), true)) {
                    $displayMessage = true;
                    $itemInfo['preorder'] = $this->getPreorderStatus($itemId);
                    if ($this->allowCompletePreorder($item, $qty)) {
                        if ($itemInfo['preorder'] == self::PREORDER_STATUS_PENDING) {
                            $itemInfo['preorder_completion_allowed'] = true;
                        }
                    }
                }

                $itemInfo["preorder_option_html"] = $this->getOptionHtml($preorderItem);
                $info[$count] = $itemInfo;
                $count++;
            }
        }

        $result['info'] = $info;
        $result['display_msg'] = $displayMessage;
        return $result;
    }

    /**
     * Check Whether Complete Preorder Is Aloowed Or Not
     *
     * @param object $item
     * @param int $qty
     *
     * @return bool
     */
    public function allowCompletePreorder($item, $qty)
    {
        $productId = $item->getProductId();
        if (!$this->isPreorderItemInvoiced($item, $qty)) {
            return false;
        }

        $stockDetails = $this->getStockDetails($productId);
        if ($stockDetails['is_in_stock'] == 1) {
            if ($stockDetails['qty'] >= $qty) {
                return true;
            }
        }

        return false;
    }

    public function isPreorderItemInvoiced($item, $qty)
    {
        $collection = $this->_preorderCollection
                            ->create()
                            ->addFieldToFilter('item_id', $item->getId())
                            ->addFieldToFilter('invoice_status', ['gteq' => 1])
                            ->addFieldToFilter('invoiced_qty', $qty);
        if ($collection->getSize()) {
            return true;
        }

        return false;
    }

    /**
     * Check Whether Message Box Is Allowed Or Not
     *
     * @return bool
     */
    public function displayMsgBoxOnCategory()
    {
        return true;
    }

    /**
     * Send Notification Mail From Admin
     *
     * @param array $productIds
     */
    public function massNotify($productIds, $ids)
    {
        $collection = $this->_productCollection
                            ->create()
                            ->addFieldToFilter("entity_id", ["in" => $productIds])
                            ->addAttributeToselect("name")
                            ->addAttributeToselect("product_url");
        foreach ($collection as $product) {
            $this->sendNotifyEmails($product, $ids);
        }
    }

    /**
     * Process Notification Mails
     *
     * @param object $product
     */
    public function sendNotifyEmails($product, $preorderIds = [])
    {
        $stores = $this->_storeManager->getStores();
        $storeId = 0;
        foreach ($stores as $store) {
            $storeId = $store->getId();
            if ($storeId > 0) {
                break;
            }
        }

        $productId = $product->getId();
        if (!$this->isAvailable($productId, 0, 1)) {
            return;
        }

        $emailIds = [];
        $collection = $this->_preorderCollection
                            ->create()
                            ->addFieldToFilter('product_id', $productId)
                            ->addFieldToFilter('preorder_status', 0)
                            ->addFieldToFilter('invoice_status', ['gteq' => 1]);
        if (count($preorderIds)) {
            $collection->addFieldToFilter('id', ['in'=> $preorderIds]);
        }
        $details = [];
        foreach ($collection as $item) {
            $emailId = $item->getBuyerEmail();
            $customerId = $item->getCustomerId();
            $orderId = $item->getOrderId();
            $orderUrl = $this->_urlBuilder->getUrl('sales/order/view/', ['order_id' => $orderId, '_secure' => false]);
            if ($customerId == 0) {
                $orderUrl = $this->_urlBuilder->getUrl('sales/guest/form/', ['_secure' => false]);
            }

            if (strpos($orderUrl, "?SID") !== false) {
                list($orderUrl, $extra) = explode("?SID", $orderUrl);
            }

            $productUrl = $product->setStoreId($storeId)->getProductUrl();
            $productName = $product->getName();
            $details[$emailId] = [
                                    "order_url" => $orderUrl,
                                    "product_url" => $productUrl,
                                    "product_name" => $productName,
                                    "customer_name" => $item->getBuyerName()
                                ];
            $this->sendEmail($details);
        }
    }

    /**
     * Send Notification Mail
     *
     * @param array $details
     */
    public function sendEmail($details)
    {
        $adminEmail = $this->getAdminEmail();
        if ($adminEmail == '') {
            return;
        }

        foreach ($details as $email => $info) {
            $area = Area::AREA_FRONTEND;
            $store = $this->_storeManager->getStore()->getId();
            $orderUrl = $info['order_url'];
            $productUrl = $info['product_url'];
            $productName = $info['product_name'];
            $customerName = $info['customer_name'];
            $templateOptions = ['area' => $area, 'store' => $store];
            $templateVars = [
                                'store' => $this->_storeManager->getStore(),
                                "order_url" => $orderUrl,
                                "product_name" => $productName,
                                "product_url" => $productUrl,
                                "customer_name" => $customerName
                            ];
            $from = ['email' => $adminEmail, 'name' => 'Store Owner'];
            $this->_inlineTranslation->suspend();
            $to = [$email];
            $templateId = $this->getTemplateId(self::CONFIG_PATH_NOTIFICATION_EMAIL);
            $transport = $this->_transportBuilder
                                ->setTemplateIdentifier($templateId)
                                ->setTemplateOptions($templateOptions)
                                ->setTemplateVars($templateVars)
                                ->setFrom($from)
                                ->addTo($to)
                                ->getTransport();
            $transport->sendMessage();
            $this->_inlineTranslation->resume();
        }
    }

    /**
     * Check Whether Reorder Is Allowed Or Not
     *
     * @param int $orderId
     *
     * @return bool
     */
    public function canReorder($orderId)
    {
        $order = $this->getOrder($orderId);
        try {
            foreach ($order->getAllItems() as $item) {
                if ($item->getSku() == "preorder_complete") {
                    return false;
                }
            }
        } catch (\Exception $e) {
            $error = $e->getMessage();
        }

        return true;
    }

    /**
     * Get Product By Product Name
     *
     * @param string $productName
     *
     * @return object
     */
    public function getProductByName($productName)
    {
        $collection = $this->_productCollection
                            ->create()
                            ->addAttributeToFilter("name", $productName);
        foreach ($collection as $product) {
            return $product;
        }

        return false;
    }

    /**
     * Check Whether Guest Is Authorised To Complete Preorder Or Not
     *
     * @return bool
     */
    public function isAuthorisedGuest()
    {
        $cartModel = $this->_cart->create();
        $quote = $cartModel->getQuote();
        foreach ($quote->getAllItems() as $item) {
            $productId = $item->getProductId();
            if ($this->isPreorderCompleteProduct($productId)) {
                $options = $this->_configuration->getCustomOptions($item);
                foreach ($options as $option) {
                    if ($option['label'] == "Order Reference") {
                        $incrementId = $option["print_value"];
                        $incrementId = str_replace("#", "", $incrementId);
                        $order = $this->_order->create()->loadByIncrementId($incrementId);
                        return true;
                    }
                }
            }
        }

        return false;
    }

    /**
     * Check Whether Quantity Is Available Or Not
     *
     * @param object $item
     *
     * @return bool
     */
    public function isQtyAllowed($item)
    {
        $productId = $item->getProductId();
        if ($this->isPreorderCompleteProduct($productId)) {
            $qty = $item->getQty();
            $options = $this->_configuration->getCustomOptions($item);
            foreach ($options as $option) {
                if ($option['label'] == "Product Name") {
                    $productName = $option["print_value"];
                    $product = $this->getProductByName($productName);
                    if ($product) {
                        $stockDetails = $this->getStockDetails($product->getId());
                        if ($product->getTypeId() == "configurable") {
                            return true;
                        }

                        if ($stockDetails['is_in_stock'] != 1) {
                            return false;
                        }

                        if ($stockDetails['qty'] < $qty) {
                            return false;
                        }
                    }
                }
            }
        }

        return true;
    }

    /**
     * Manage Quantity After Shipment For Complete Payment Preorder
     *
     * @param int $itemId
     * @param int $qty
     */
    public function processShipmenmt($itemId, $qty)
    {
        $collection = $this->_preorderCollection
                            ->create()
                            ->addFieldToFilter('item_id', $itemId)
                            ->addFieldToFilter('type', 0);
        if ($collection->getSize()) {
            foreach ($collection as $item) {
                $this->updateStockData($item->getProductId(), $qty, true);
                break;
            }
        }
    }

    public function getTotalOriginalPriceByItem($item)
    {
        $price = $this->getOriginalPriceByItem($item);
        $qty = $item->getQty();
        return $price*$qty;
    }

    public function checkShipment()
    {
        $result = ["error" => false];
        $data = $this->_request->getParams();
        if (array_key_exists("shipment", $data)) {
            if (array_key_exists("items", $data['shipment'])) {
                $items = $data['shipment']['items'];
                foreach ($items as $itemId => $qty) {
                    if (!$qty) {
                        continue;
                    }

                    if (!$this->allowShipment($itemId)) {
                        $result['error'] = true;
                        $result['msg'] = __("Preorder items can not be shipped before complete payment.");
                        return $result;
                    }
                }
            }
        }

        return $result;
    }

    public function allowShipment($itemId)
    {
        $collection = $this->_preorderCollection->create();
        $sql = "item_id = $itemId OR parent_item_id = $itemId";
        $collection->getSelect()->where($sql);
        if ($collection->getSize()) {
            foreach ($collection as $item) {
                if ($item->getType() == self::PREORDER_TYPE_PARTIAL) {
                    if ($item->getPreorderStatus() == self::PREORDER_STATUS_COMPLETE) {
                        return true;
                    }

                    return false;
                } else {
                    return true;
                }
            }
        }

        return true;
    }

    public function getOptionHtml($item)
    {
        if ($item) {
            $html = "";
            $html .= "<div class='wk-preorder-info'>";
            $html .= "<div class='wk-preorder-info-title'>Preorder Information</div>";
            $html .= "<div class='wk-preorder-info-items'>";
            $html .= "<div class='wk-preorder-info-item'>";
            $html .= "<div class='wk-preorder-info-item-label'>Type :</div>";
            if ($item->getType() == self::PREORDER_TYPE_COMPLETE) {
                $label = self::PREORDER_TYPE_COMPLETE_LABEL;
            } else {
                $label = self::PREORDER_TYPE_PARTIAL_LABEL;
            }

            $html .= "<div class='wk-preorder-info-item-value'>".$label."</div>";
            $html .= "</div>";
            $html .= "<div class='wk-preorder-info-item'>";
            $status = $item->getPreorderStatus();
            $html .= "<div class='wk-preorder-info-item-label'>Status :</div>";
            if ($status == self::PREORDER_STATUS_PENDING) {
                $label = self::PREORDER_STATUS_PENDING_LABEL;
            } elseif ($status == self::PREORDER_STATUS_PROCESSING) {
                $label = self::PREORDER_STATUS_PROCESSING_LABEL;
            } else {
                $label = self::PREORDER_STATUS_COMPLETE_LABEL;
            }

            $html .= "<div class='wk-preorder-info-item-value'>".$label."</div>";
            $html .= "</div>";
            $html .= "</div>";
            $html .= "</div>";
            return $html;
        }

        return "";
    }

    public function getPreorderNote()
    {
        return __("This order contains Preorder Products.");
    }

    /**
     * Manage Preorder Status From Complete Preorder Product
     *
     * @param \Magento\Sales\Model\Order\Item $orderItem
     * @param boolean $needCalculation
     * @return void
     */
    public function managePreorderStatus($orderItem, $needCalculation = true)
    {
        $orderItemId = $orderItem->getId();
        $quoteItemId = $orderItem->getQuoteItemId();
        $collection = $this->_completeCollection->create();
        $value = $quoteItemId;
        $field = 'quote_item_id';
        $completeItem = $this->getDataByField($value, $field, $collection);
        if ($completeItem) {
            $itemId = $completeItem->getOrderItemId();
            $collection = $this->_preorderCollection->create();
            $field = 'item_id';
            $preorderItem = $this->getDataByField($itemId, $field, $collection);
            if ($preorderItem) {
                if ($needCalculation) {
                    $preorderStatus = self::PREORDER_STATUS_PROCESSING;
                    $invoiceCollection = $this->_invoiceItemCollection
                                            ->create()
                                            ->addFieldToFilter('order_item_id', $orderItemId);
                    $previousInvoicedQty = 0;
                    $currentInvoicedQty = 0;
                    foreach ($invoiceCollection as $invoiceItem) {
                        $previousInvoicedQty += $invoiceItem->getQty();
                    }

                    $data = $this->_request->getParam("invoice");
                    if (array_key_exists("items", $data)) {
                        if (array_key_exists($orderItemId, $data['items'])) {
                            $currentInvoicedQty = $data['items'][$orderItemId];
                        }
                    }

                    $totalInvoicedQty = $previousInvoicedQty + $currentInvoicedQty;
                    if ($totalInvoicedQty == $preorderItem->getInvoicedQty()) {
                        $preorderStatus = self::PREORDER_STATUS_COMPLETE;
                    }
                } else {
                    $preorderStatus = self::PREORDER_STATUS_COMPLETE;
                }

                $preorderItem->setPreorderStatus($preorderStatus)
                            ->setId($preorderItem->getId())
                            ->save();
            }
        }
    }

    /**
     * updatePreorderInvoiceStatus
     *
     * @param \Webkul\Preorder\Model\Preorder $item
     * @param int $totalInvoicedQty
     */
    public function updatePreorderInvoiceStatus($item, $totalInvoicedQty)
    {
        $data = ['invoiced_qty' => $totalInvoicedQty, 'invoice_status' => 1];
        if ($item->getType() == self::PREORDER_TYPE_COMPLETE) {
            $data['preorder_status'] = self::PREORDER_STATUS_PROCESSING;
            if ($item->getQty() == $totalInvoicedQty) {
                $data['preorder_status'] = self::PREORDER_STATUS_COMPLETE;
            }
        }

        $item->addData($data)->setId($item->getId())->save();
    }

    /**
     * Return template id
     *
     * @return mixed
     */
    public function getTemplateId($path)
    {
        return $this->_scopeConfig->getValue(
            $path,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * Get Remaining Price For Complete Prorder Item
     *
     * @param object $preorderItem
     *
     * @return float
     */
    public function getUnitPrice($preorderItem)
    {
        $currentCurrencyCode = $this->_storeManager->getStore()->getCurrentCurrencyCode();
        $baseCurrencyCode = $preorderItem->getBaseCurrencyCode();
        $orderCurrencyCode = $preorderItem->getOrderCurrencyCode();
        $unitPrice = $preorderItem->getRemainingAmount();
        if ($currentCurrencyCode == $orderCurrencyCode) {
            return $unitPrice;
        }

        $unitPrice = $this->convertPriceToBase($unitPrice, $orderCurrencyCode);
        $unitPrice = $this->convertPriceFromBase($unitPrice, $currentCurrencyCode);
        return $unitPrice;
    }

    /**
     * Convert Base Price To Currency Price
     *
     * @param float $amount
     * @param string $currency
     *
     * @return float
     */
    public function convertPriceFromBase($amount, $currency)
    {
        $store = $this->_storeManager->getStore();
        return $store->getBaseCurrency()->convert($amount, $currency);
    }

    /**
     * Convert Price To Base Currency Price
     *
     * @param float $amount
     * @param string $currency
     *
     * @return float
     */
    public function convertPriceToBase($amount, $currency)
    {
        $rate = $this->getCurrencyRateToBase($currency);
        $amount = $amount * $rate;
        return $amount;
    }

    /**
     * Get Currency Rate To Base Currency
     *
     * @param string $currency
     *
     * @return float
     */
    public function getCurrencyRateToBase($currency)
    {
        $store = $this->_storeManager->getStore();
        $baseCurrencyCode = $store->getBaseCurrency()->getCode();
        $rate = $this->_currencyFactory->create()
                    ->load($currency)
                    ->getAnyRate($baseCurrencyCode);
        return $rate;
    }

    /**
     * Get Product's Purchased Options Info
     *
     * @param Magento\Sales\Model\Order\Item $item
     *
     * @return array
     */
    public function getOptionsInfo($item)
    {
        $result = "";
        $customOptions = [];
        $data = $item->getProductOptions();
        $configurableOptions = [];
        $hasCustomOptions = false;
        $hasConfigOptions = false;

        try {
            if (array_key_exists("options", $data)) {
                foreach ($data['options'] as $option) {
                    $hasCustomOptions = true;
                    $customOptions[] = $option['label'].":".$option['print_value'];
                }
            }
        } catch (\Exception $e) {
            $hasCustomOptions = false;
        }
        
        try {
            if (array_key_exists("attributes_info", $data)) {
                foreach ($data['attributes_info'] as $option) {
                    $hasConfigOptions = true;
                    $configurableOptions[] = $option['label'].":".$option['value'];
                }
            }
        } catch (\Exception $e) {
            $hasConfigOptions = false;
        }

        if ($hasCustomOptions) {
            $result .= "Custom Options => ".implode(", ", $customOptions);
        }

        if ($hasConfigOptions) {
            if ($hasCustomOptions) {
                $result .= " | Configurable Options => ".implode(", ", $configurableOptions);
            } else {
                $result .= "Configurable Options => ".implode(", ", $configurableOptions);
            }
        }

        return $result;
    }

    public function allowProductInCart($product)
    {
        $productId = $product->getId();
        if ($this->_moduleManager->isEnabled('Webkul_Walletsystem')) {
            $walletHelper = $this->_objectManager->create('Webkul\Walletsystem\Helper\Data');
            $walletProductId = $walletHelper->getWalletProductId();
            if ($productId == $walletProductId) {
                return true;
            }
        }

        $preorderCompleteProductId = $this->getPreorderCompleteProductId();
        if ($productId == $preorderCompleteProductId) {
            return true;
        }

        return false;
    }

    public function allowRefreshItemPrice($item)
    {
        if ($this->_moduleManager->isEnabled('Webkul_Walletsystem')) {
            $productId = $item->getProductId();
            $walletHelper = $this->_objectManager->create('Webkul\Walletsystem\Helper\Data');
            $walletProductId = $walletHelper->getWalletProductId();
            if ($productId == $walletProductId) {
                return false;
            }
        }
        if ($this->_moduleManager->isEnabled('Webkul_MpAssignProduct') && $item->getItemId()) {
            $mpassignHelper = $this->_objectManager->create('Webkul\MpAssignProduct\Helper\Data');
            $assignData = $mpassignHelper->getAssignDataByItemId($item->getItemId());
            if ($assignData['assign_id']) {
                return false;
            }
        }
        $assignData = $this->_request->getParams();
        if (isset($assignData['mpassignproduct_id'])) {
            return false;
        }
        return true;
    }

    /**
     * Check Preorder Product Qty.
     * @param  $item
     * @param  \Magento\Catalog\Model\Product  $product
     * @return bool
     */
    public function getQtyCheck($item, $product)
    {
        $productType = $product->getTypeId();
        if ($productType == 'configurable') {
            $configModel = $this->_configurable;
            $usedProductIds = $configModel->getUsedProductIds($product);
            foreach ($usedProductIds as $usedProductId) {
                return $this->checkPreorderAvailable($usedProductId, $item);
            }
        } else {
            return $this->checkPreorderAvailable($product->getId(), $item);
        }
        return true;
    }

    /**
     * Check Product Availability for Preorder.
     * @int  $productId
     * @return bool
     */
    public function checkPreorderAvailable($productId, $item)
    {
        $product = $this->_catalogProduct->load($productId);
        $maxPreorderQty = $product->getWkPreorderQty();
        if ($maxPreorderQty) {
            $pendingQty = 0;
            $itemCollection = $this->_preorderCollection->create();
            $itemCollection->addFieldToFilter('product_id', $product->getId())
                            ->addFieldToFilter('preorder_status', ['neq'=> 2]);
            $pendingOrderCount = $itemCollection->getSize();
            if ($pendingOrderCount) {
                foreach ($itemCollection as $key) {
                    $pendingQty += $key->getQty();
                }
            }
            if (($maxPreorderQty - $pendingQty) < $item->getQty()) {
                return false;
            }
        }
        return true;
    }

    /**
     * getPreorderFee function
     *
     * @return float
     */
    public function getPreorderFee()
    {
        $fee = 0;
        $global = true;
        if ($this->isLoggedIn()) {
            $customerId = $this->_customerSession->getCustomerId();
            $customer = $this->_customerRepository->getById($customerId);
            $attribute = $customer->getCustomAttributes();
            if (array_key_exists("custom_preorder_fee", $attribute)) {
                $custom = $attribute['custom_preorder_fee']->getValue();
                if ($custom && array_key_exists("preorder_fee", $attribute)) {
                    $fee = (float)$attribute['preorder_fee']->getValue();
                    $global = false;
                }
            }
        }
        if ($global) {
            $config = 'preorder/settings/preorder_additional_fee';
            $fee = (float) $this->_scopeConfig->getValue(
                $config,
                \Magento\Store\Model\ScopeInterface::SCOPE_WEBSITES
            );
        }
        $currentCurrencyCode = $this->_storeManager->getStore()->getCurrentCurrencyCode();
        $fee = $this->convertPriceFromBase($fee, $currentCurrencyCode);
        return $fee;
    }

    /**
     * get Preorder Fee Calculation Method
     *
     * @return string
     */
    public function getPreorderFeeType() {
        if ($this->isLoggedIn()) {
            $customerId = $this->_customerSession->getCustomerId();
            $customer = $this->_customerRepository->getById($customerId);
            $attribute = $customer->getCustomAttributes();
            if (array_key_exists("custom_preorder_fee", $attribute)) {
                $custom = $attribute['custom_preorder_fee']->getValue();
                if ($custom && array_key_exists("preorder_fee_type", $attribute)) {
                    $feeType = $attribute['preorder_fee_type']->getValue();
                    return $feeType;
                }
            }
        }
        $config = 'preorder/settings/preorder_fee_type';
        $feeType = $this->_scopeConfig->getValue(
            $config,
            \Magento\Store\Model\ScopeInterface::SCOPE_WEBSITES
        );
        return $feeType;
    }

    /**
     * Get list of last ordered products
     *
     * @return array
     */
    public function getItems($order, $limit)
    {
        $items = [];
        
        if ($order) {
            $website = $this->_storeManager->getStore()->getWebsiteId();
            /*
                * @var \Magento\Sales\Model\Order\Item $item
            */
            foreach ($order->getParentItemsRandomCollection($limit) as $item) {
                try {
                    $product = $this->productRepository->getById(
                        $item->getProductId(),
                        false,
                        $this->_storeManager->getStore()->getId()
                    );
                } catch (NoSuchEntityException $noEntityException) {
                    $this->logger->critical($noEntityException);
                    continue;
                }

                if ($item->hasData('product') && in_array($website, $item->getProduct()->getWebsiteIds())) {
                    if (!$this->isPreorderCompleteProduct($item->getProduct(), true)) {
                        $items[] = [
                                    'id'          => $item->getId(),
                                    'name'        => $item->getName(),
                                    'url'         => $item->getProduct()->getProductUrl(),
                                    'is_saleable' => $this->isItemAvailableForReorder($item),
                                   ];
                    }
                }
            }
        }
        return $items;
    }

    public function getProductByRepository($productId) {
        return $this->productRepository->getById(
            $productId,
            false,
            $this->_storeManager->getStore()->getId()
        );
    }

    /**
     * Check item product availability for reorder
     *
     * @param  \Magento\Sales\Model\Order\Item $orderItem
     * @return boolean
     */
    protected function isItemAvailableForReorder(\Magento\Sales\Model\Order\Item $orderItem)
    {
        try {
            $stockItem = $this->_stockRegistry->getStockItem(
                $orderItem->getProduct()->getId(),
                $orderItem->getStore()->getWebsiteId()
            );
            return $stockItem->getIsInStock();
        } catch (NoSuchEntityException $noEntityException) {
            return false;
        }
    }
}
