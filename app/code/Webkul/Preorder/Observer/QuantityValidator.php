<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Preorder
 * @author    Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\Preorder\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\CatalogInventory\Helper\Data;

class QuantityValidator implements ObserverInterface
{
    /**
     * @var \Webkul\Preorder\Helper\Data
     */
    protected $_preorderHelper;

    /**
     * @param \Webkul\Preorder\Helper\Data $preorderHelper
     */
    public function __construct(
        \Webkul\Preorder\Helper\Data $preorderHelper
    ) {
        $this->_preorderHelper = $preorderHelper;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $quoteItem = $observer->getEvent()->getItem();
        if (!$this->_preorderHelper->isQtyAllowed($quoteItem)) {
            $quoteItem->addErrorInfo('cataloginventory', Data::ERROR_QTY, __('Product is not available.'));
            $quoteItem->getQuote()
                    ->addErrorInfo('stock', 'cataloginventory', Data::ERROR_QTY, __('Product is not available.'));
        }
    }
}
