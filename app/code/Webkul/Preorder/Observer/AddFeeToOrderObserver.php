<?php
namespace Webkul\Preorder\Observer;

use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;

class AddFeeToOrderObserver implements ObserverInterface
{
    /**
     * Set payment fee to order
     *
     * @param EventObserver $observer
     * @return $this
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $quote = $observer->getQuote();
        $fee = $quote->getPreorderFee();
        
        if (!$fee) {
            return $this;
        }
        //Set fee data to order
        $order = $observer->getOrder();
        $order->setData('preorder_fee', $fee);
        return $this;
    }
}
