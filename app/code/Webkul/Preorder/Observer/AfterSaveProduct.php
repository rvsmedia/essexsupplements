<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Preorder
 * @author    Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\Preorder\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\RequestInterface;

class AfterSaveProduct implements ObserverInterface
{
    /**
     * @var RequestInterface
     */
    protected $_request;

    /**
     * @var \Webkul\Preorder\Helper\Data
     */
    protected $_preorderHelper;

    /**
     * @param RequestInterface $request
     * @param \Webkul\Preorder\Helper\Data $preorderHelper
     */
    public function __construct(
        RequestInterface $request,
        \Webkul\Preorder\Helper\Data $preorderHelper
    ) {
        $this->_request = $request;
        $this->_preorderHelper = $preorderHelper;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $helper = $this->_preorderHelper;
        $product = $observer->getProduct();
        if ($helper->getAutoEmail()) {
            $isInStock = 0;
            $data = $this->_request->getParams();
            $stockData = $data['product']['quantity_and_stock_status'];
            if (array_key_exists('is_in_stock', $stockData)) {
                $isInStock = $stockData['is_in_stock'];
            }

            if ($isInStock == 1) {
                $helper->sendNotifyEmails($product);
            }
        }
    }
}
