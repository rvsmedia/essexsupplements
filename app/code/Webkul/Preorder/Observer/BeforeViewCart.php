<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Preorder
 * @author    Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\Preorder\Observer;

use Magento\Framework\Event\ObserverInterface;

class BeforeViewCart implements ObserverInterface
{
    /**
     * @var \Webkul\Preorder\Helper\Data
     */
    protected $_preorderHelper;

    /**
     * @param \Webkul\Preorder\Helper\Data $helper
     */
    public function __construct(\Webkul\Preorder\Helper\Data $helper)
    {
        $this->_preorderHelper = $helper;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $this->_preorderHelper->checkStatus();
    }
}
