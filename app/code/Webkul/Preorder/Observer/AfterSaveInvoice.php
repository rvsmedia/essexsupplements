<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Preorder
 * @author    Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\Preorder\Observer;

use Magento\Framework\Event\ObserverInterface;
use Webkul\Preorder\Model\ResourceModel\Preorder\CollectionFactory;
use Webkul\Preorder\Helper\Data as PreorderHelper;

class AfterSaveInvoice implements ObserverInterface
{
    /**
     * @var \Webkul\Preorder\Helper\Data
     */
    protected $_preorderHelper;

    /**
     * @var \Webkul\Preorder\Model\ResourceModel\Preorder\CollectionFactory
     */
    protected $_preorderCollection;

    /**
     * @param Preorder $preorderCollection
     */
    public function __construct(
        \Webkul\Preorder\Helper\Data $preorderHelper,
        CollectionFactory $preorderCollection
    ) {
        $this->_preorderHelper = $preorderHelper;
        $this->_preorderCollection = $preorderCollection;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        try {
            $invoice = $observer->getEvent()->getInvoice();
            $order = $invoice->getOrder();
            $collection = $this->_preorderCollection
                                ->create()
                                ->addFieldToFilter("order_id", $order->getId());
            foreach ($collection as $item) {
                $qty = $item->getQty();
                $this->_preorderHelper->updatePreorderInvoiceStatus($item, $qty);
            }

            foreach ($order->getAllItems() as $item) {
                if ($this->_preorderHelper->isPreorderCompleteProduct($item->getProduct(), true)) {
                    $this->_preorderHelper->managePreorderStatus($item, false);
                }
            }
        } catch (\Exception $e) {
            $e->getMessage();
        }
    }
}
