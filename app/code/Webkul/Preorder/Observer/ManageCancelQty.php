<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Preorder
 * @author    Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\Preorder\Observer;

use Magento\Framework\Event\ObserverInterface;
use Webkul\Preorder\Helper\Data as PreorderHelper;
use Webkul\Preorder\Model\ResourceModel\Preorder\CollectionFactory as Preorder;

class ManageCancelQty implements ObserverInterface
{

    /**
     * @var \Webkul\Preorder\Helper\Data
     */
    protected $_preorderHelper;

    /**
     * @var Preorder
     */
    protected $_preorderCollection;

    /**
     * @param \Webkul\Preorder\Helper\Data $preorderHelper
     * @param Preorder $preorderCollection
     */
    public function __construct(
        \Webkul\Preorder\Helper\Data $preorderHelper,
        Preorder $preorderCollection
    ) {
        $this->_preorderHelper = $preorderHelper;
        $this->_preorderCollection = $preorderCollection;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $helper = $this->_preorderHelper;
        $order = $observer->getEvent()->getOrder();
        if ($order->getOrderType() == '1') {
            $orderedItems = $order->getAllItems();
            foreach ($orderedItems as $item) {
                $collection  = $this->_preorderCollection->create()
                            ->addFieldToFilter('item_id', $item->getId());
                if ($collection->getSize()) {
                    $helper->updateStockData($item->getProductId(), $item->getQtyOrdered(), true);
                }
            }
        }
    }
}
