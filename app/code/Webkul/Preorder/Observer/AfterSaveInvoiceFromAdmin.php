<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Preorder
 * @author    Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\Preorder\Observer;

use Magento\Framework\Event\ObserverInterface;
use Webkul\Preorder\Helper\Data as PreorderHelper;
use Webkul\Preorder\Model\ResourceModel\Item\CollectionFactory as Items;
use Webkul\Preorder\Model\ResourceModel\Complete\CollectionFactory;
use Webkul\Preorder\Model\ResourceModel\Preorder\CollectionFactory as Preorder;

class AfterSaveInvoiceFromAdmin implements ObserverInterface
{
    /**
     * @var \Webkul\Preorder\Helper\Data
     */
    protected $_preorderHelper;

    /**
     * @var Preorder
     */
    protected $_preorderCollection;

    /**
     * @param \Webkul\Preorder\Helper\Data $preorderHelper
     * @param Preorder $preorderCollection
     */
    public function __construct(
        \Webkul\Preorder\Helper\Data $preorderHelper,
        Preorder $preorderCollection
    ) {
        $this->_preorderHelper = $preorderHelper;
        $this->_preorderCollection = $preorderCollection;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $invoice = $observer->getEvent()->getInvoice();
        try {
            $data = $this->_preorderHelper->getParams();
            $itemIds = $data['invoice']['items'];
            $ids = [];
            foreach ($itemIds as $itemId => $qty) {
                if ($qty > 0) {
                    $ids[] = $itemId;
                }
            }

            $mapping = [];
            $orderItemIds = [];
            $orderParentItemIds = [];
            $qtys = [];
            foreach ($invoice->getAllItems() as $item) {
                $itemId = $item->getOrderItemId();
                $qty = $item->getQty();
                if (in_array($itemId, $ids)) {
                    $orderItemIds[] = $itemId;
                    $qtys[$itemId] = $qty;
                }

                if ($parent = $item->getOrderItem()->getParentItem()) {
                    $itemId = $parent->getItemId();
                    if (in_array($itemId, $ids)) {
                        $mapping[$item->getOrderItemId()] = $itemId;
                        $qtys[$item->getOrderItemId()] = $qty;
                        $orderParentItemIds[] = $itemId;
                        $orderItemIds[] = $item->getOrderItemId();
                    }
                }
            }

            $finalItemIds = array_diff($orderItemIds, $orderParentItemIds);
            $collection = $this->_preorderCollection
                                ->create()
                                ->addFieldToFilter("item_id", ["in" => $finalItemIds]);

            foreach ($collection as $item) {
                $invoicedQty = $item->getInvoicedQty();
                $orderItemId = $item->getItemId();
                $key = $orderItemId;
                if (array_key_exists($orderItemId, $mapping)) {
                    $key = $mapping[$orderItemId];
                }

                $qty = $qtys[$key];
                $qty += $invoicedQty;
                $this->_preorderHelper->updatePreorderInvoiceStatus($item, $qty);
            }

            $order = $invoice->getOrder();
            foreach ($order->getAllItems() as $item) {
                if ($this->_preorderHelper->isPreorderCompleteProduct($item->getProduct(), true)) {
                    $this->_preorderHelper->managePreorderStatus($item);
                }
            }
        } catch (\Exception $e) {
            $e->getMessage();
        }
    }
}
