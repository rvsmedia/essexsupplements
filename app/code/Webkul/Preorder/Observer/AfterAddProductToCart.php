<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Preorder
 * @author    Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\Preorder\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\RequestInterface;
use Webkul\Preorder\Model\ResourceModel\Item\CollectionFactory;

class AfterAddProductToCart implements ObserverInterface
{
    /**
     * @var RequestInterface
     */
    protected $_request;

    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $_customerSession;

    /**
     * @var \Magento\Framework\Message\ManagerInterface
     */
    protected $_messageManager;

    /**
     * @var \Magento\Checkout\Model\CartFactory
     */
    protected $_cart;

    /**
     * @var \Webkul\Preorder\Model\ItemFactory
     */
    protected $_item;

    /**
     * @var \Webkul\Preorder\Model\CompleteFactory
     */
    protected $_complete;

    /**
     * @var \Webkul\Preorder\Helper\Data
     */
    protected $_preorderHelper;

    /**
     * @var CollectionFactory
     */
    protected $_itemCollection;

    /**
     * @param RequestInterface $request
     * @param \Magento\Customer\Model\Session $customerSession
     * @param \Magento\Framework\Message\ManagerInterface $messageManager
     * @param \Magento\Checkout\Model\CartFactory $cart
     * @param \Webkul\Preorder\Model\ItemFactory $item
     * @param \Webkul\Preorder\Model\CompleteFactory $complete
     * @param \Webkul\Preorder\Helper\Data $preorderHelper
     * @param CollectionFactory $itemCollection
     */
    public function __construct(
        RequestInterface $request,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Magento\Checkout\Model\CartFactory $cart,
        \Webkul\Preorder\Model\ItemFactory $item,
        \Webkul\Preorder\Model\CompleteFactory $complete,
        \Webkul\Preorder\Helper\Data $preorderHelper,
        CollectionFactory $itemCollection
    ) {
        $this->_request = $request;
        $this->_customerSession = $customerSession;
        $this->_messageManager = $messageManager;
        $this->_cart = $cart;
        $this->_item = $item;
        $this->_complete = $complete;
        $this->_preorderHelper = $preorderHelper;
        $this->_itemCollection = $itemCollection;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $helper = $this->_preorderHelper;
        $cartModel = $this->_cart->create();
        $itemId = 0;
        $productId = 0;
        $preorderPercent = $helper->getPreorderPercent();
        $cart = $cartModel->getQuote();
        foreach ($cart->getAllItems() as $item) {
            $itemId = $item->getId();
            $itemPrice = $item->getPrice();
            $productId = $item->getProductId();
        }

        if ($itemId > 0 && $productId > 0) {
            if ($helper->isPreorder($productId)) {
                $collection = $this->_itemCollection->create();
                $field = 'item_id';
                $item = $helper->getDataByField($itemId, $field, $collection);
                if (!$item) {
                    $data = [
                                'item_id' => $itemId,
                                'preorder_percent' => $preorderPercent
                            ];
                    $this->_item->create()->setData($data)->save();
                }
            }
        }

        $preorderCompleteProductId = $helper->getPreorderCompleteProductId();
        if ($productId == $preorderCompleteProductId) {
            $customerId = 0;
            if ($this->_customerSession->isLoggedIn()) {
                $customerId = (int) $this->_customerSession->getCustomerId();
            }

            $data = $this->_request->getParams();
            $qty = $data['qty'];
            $orderId = $data['order_id'];
            $orderItemId = $data['item_id'];
            $preorderProductId = $data['pro_id'];
            $completeData = [
                                'order_id' => $orderId,
                                'order_item_id' => $orderItemId,
                                'customer_id' => $customerId,
                                'product_id' => $preorderProductId,
                                'quote_item_id' => $itemId,
                                'qty' => $qty
                            ];
            $this->_complete->create()->addData($completeData)->save();
        }
    }
}
