<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Preorder
 * @author    Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\Preorder\Observer;

use Magento\Framework\Event\ObserverInterface;
use Webkul\Preorder\Helper\Data as PreorderHelper;
use Webkul\Preorder\Model\ResourceModel\Item\CollectionFactory as Items;
use Webkul\Preorder\Model\ResourceModel\Complete\CollectionFactory;
use Webkul\Preorder\Model\ResourceModel\Preorder\CollectionFactory as Preorder;

class AfterPlaceOrder implements ObserverInterface
{
    protected $_orderHasInvoice = false;

    /**
     * @var \Webkul\Preorder\Helper\Data
     */
    protected $_preorderHelper;

    /**
     * @var \Webkul\Preorder\Model\PreorderFactory
     */
    protected $_preorder;

    /**
     * @var Items
     */
    protected $_itemCollection;

    /**
     * @var CollectionFactory
     */
    protected $_completeCollection;

    /**
     * @var Preorder
     */
    protected $_preorderCollection;

    /**
     * @param \Webkul\Preorder\Helper\Data $preorderHelper
     * @param \Webkul\Preorder\Model\PreorderFactory $preorder
     * @param Items $itemCollection
     * @param CollectionFactory $completeCollection
     * @param Preorder $preorderCollection
     */
    public function __construct(
        \Webkul\Preorder\Helper\Data $preorderHelper,
        \Webkul\Preorder\Model\PreorderFactory $preorder,
        \Magento\Quote\Model\Quote\ItemFactory $quoteItem,
        Items $itemCollection,
        CollectionFactory $completeCollection,
        Preorder $preorderCollection
    ) {
        $this->_preorderHelper = $preorderHelper;
        $this->_preorder = $preorder;
        $this->_quoteItem = $quoteItem;
        $this->_itemCollection = $itemCollection;
        $this->_completeCollection = $completeCollection;
        $this->_preorderCollection = $preorderCollection;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $helper = $this->_preorderHelper;
        $orderIds = $observer->getEvent()->getData('order_ids');
        $orderId = $orderIds[0];
        $order = $helper->getOrder($orderId);
        if ($order->hasInvoices()) {
            $this->_orderHasInvoice = true;
        }
        $orderedItems = $order->getAllItems();
        foreach ($orderedItems as $item) {
            $this->setPreorderData($item, $order);
            $this->setPreorderCompleteData($item);
        }
    }

    /**
     * Set Preorder Price and Data in Table
     *
     * @param object $orderItem
     * @param object $order
     */
    public function setPreorderData($orderItem, $order)
    {
        $helper = $this->_preorderHelper;
        $preorderType = $helper->getPreorderType();
        $time = time();
        $customerId = (int) $order->getCustomerId();
        $customerEmail = $order->getCustomerEmail();
        $remainingAmount = 0;
        $preorderPercent = 100;
        $parent = ($orderItem->getParentItem() ? $orderItem->getParentItem() : $orderItem);
        $parentId = $parent->getProductId();
        $parentItemId = 0;
        $productId = $orderItem->getProductId();
        $quoteItemId = $orderItem->getQuoteItemId();
        if ($parentId == $productId) {
            $parentId = 0;
        }

        if ($helper->isPreorderOrderedItem($orderItem->getQuoteItemId())) {
            $order->setOrderType(1)->save();
            $orderItemId = $orderItem->getId();
            $parentItemId = $orderItem->getParentItemId();
            $qty = $orderItem->getQtyOrdered();
            $price = $parent->getPrice();
            if ($helper->isPartialPreorder($productId)) {
                $collection = $this->_itemCollection->create();
                $value = $quoteItemId;
                $field = 'item_id';
                $preorderItem = $helper->getDataByField($value, $field, $collection);
                if ($preorderItem) {
                    $preorderPercent = $preorderItem->getPreorderPercent();
                    if ($preorderPercent > 0) {
                        $totalPrice = ($price * 100) / $preorderPercent;
                        $remainingAmount = $totalPrice - $price;
                    } else {
                        $remainingAmount = $this->getRemainingAmount($parent);
                    }
                }
            }

            $buyerName = $this->getBuyerNameFromOrder($order);
            $taxPercent = $orderItem->getTaxPercent();
            $totalPaidAmount = $qty * $price;
            $totalRemainingAmount = $qty * $remainingAmount;
            $preorderItemData = [
                                    'order_id' => $order->getId(),
                                    'item_id' => $orderItemId,
                                    'parent_item_id' => $parentItemId,
                                    'product_id' => $productId,
                                    'parent_id' => $parentId,
                                    'customer_id' => $customerId,
                                    'buyer_email' => $customerEmail,
                                    'buyer_name' => $buyerName,
                                    'preorder_percent' => $preorderPercent,
                                    'paid_amount' => $price,
                                    'remaining_amount' => $remainingAmount,
                                    'total_paid_amount' => $totalPaidAmount,
                                    'total_remaining_amount' => $totalRemainingAmount,
                                    'qty' => $qty,
                                    'type' => $preorderType,
                                    'preorder_status' => 0,
                                    'time' => $time,
                                    'base_currency_code' => $order->getBaseCurrencyCode(),
                                    'order_currency_code' => $order->getOrderCurrencyCode(),
                                    'product_name' => $orderItem->getName()
                                ];

            if ($taxPercent > 0 && $preorderPercent < 100) {
                $totalTaxAmount = $orderItem->getTaxAmount();
                $taxAmount = $totalTaxAmount/$qty;
                $totalprice = $remainingAmount + $price;
                $totalTax = ($totalprice * $taxPercent)/100;
                $remainingTaxAmount = $totalTax - $taxAmount;
                $preorderItemData["tax_percent"] = $taxPercent;
                $preorderItemData["paid_tax_amount"] = $taxAmount;
                $preorderItemData["remaining_tax_amount"] = $remainingTaxAmount;
                $preorderItemData["paid_amount"] = $price + $taxAmount;
                $preorderItemData["remaining_amount"] = $remainingAmount + $remainingTaxAmount;
                $preorderItemData["total_paid_amount"] = $qty * ($price + $taxAmount);
                $preorderItemData["total_remaining_amount"] = $qty * ($remainingAmount + $remainingTaxAmount);
            }

            if ($this->_orderHasInvoice) {
                $preorderItemData["invoiced_qty"] = $qty;
                $preorderItemData["invoice_status"] = 1;
            }

            if ($this->_orderHasInvoice && $preorderType == PreorderHelper::PREORDER_TYPE_COMPLETE) {
                $preorderItemData["preorder_status"] = PreorderHelper::PREORDER_STATUS_COMPLETE;
            }

            $this->_preorder->create()->setData($preorderItemData)->save();
            try {
                $helper->updateStockData($productId, $qty);
            } catch (\Exception $e) {
                $error = $e->getMessage();
            }
        }
    }

    /**
     * Get Buyer Name From Order
     *
     * @param \Magento\Sales\Model|Order $order
     *
     * @return string
     */
    public function getBuyerNameFromOrder($order)
    {
        if ($order->getCustomerIsGuest()) {
            $address = $order->getBillingAddress();
            $buyerName = $address->getFirstname()." ".$address->getLastname();
        } else {
            $buyerName = $order->getCustomerFirstname()." ".$order->getCustomerLastname();
        }

        return $buyerName;
    }

    /**
     * Set Preorder Complete Price and Data in Table
     *
     * @param object $orderItem
     */
    public function setPreorderCompleteData($orderItem)
    {
        $helper = $this->_preorderHelper;
        $quoteItemId = $orderItem->getQuoteItemId();
        $productId = $orderItem->getProductId();
        $preorderCompleteProductId = $helper->getPreorderCompleteProductId();
        if ($productId == $preorderCompleteProductId) {
            $id = 0;
            $collection = $this->_completeCollection->create();
            $value = $quoteItemId;
            $field = 'quote_item_id';
            $item = $helper->getDataByField($value, $field, $collection);
            if ($item) {
                $itemId = $item->getOrderItemId();
                $collection = $this->_preorderCollection->create();
                $field = 'item_id';
                $item = $helper->getDataByField($itemId, $field, $collection);
                if ($item) {
                    $remainingAmount = $item->getRemainingAmount();
                    $paidAmount = $item->getPaidAmount();
                    $totalAmount = $paidAmount + $remainingAmount;
                    if ($this->_orderHasInvoice) {
                        $preorderStatus = PreorderHelper::PREORDER_STATUS_COMPLETE;
                    } else {
                        $preorderStatus = PreorderHelper::PREORDER_STATUS_PROCESSING;
                    }

                    $item->setPreorderStatus($preorderStatus)
                        ->setRemainingAmount(0)
                        ->setPaidAmount($totalAmount)
                        ->setId($item->getId())
                        ->save();
                }
            }

            $productOptions = $orderItem->getProductOptions();
            $options = $productOptions['options'];
            foreach ($options as $option) {
                if ($option['label'] == "Product Name") {
                    $productName = $option["print_value"];
                    $product = $helper->getProductByName($productName);
                    try {
                        $helper->updateStockData($product->getId(), $orderItem->getQtyOrdered(), true);
                    } catch (\Exception $e) {
                        $error = $e->getMessage();
                    }
                }
            }
        }
    }

    public function getOptionPrice($option, $value, $productPrice, $multiOption = false)
    {
        $totalPrice = 0;
        if ($multiOption) {
            foreach ($option->getValues() as $v) {
                if (in_array($v->getOptionTypeId(), $value)) {
                    $optionPrice = $v->getPrice();
                    $priceType = $v->getPriceType();
                    if ($priceType != "fixed") {
                        $optionPrice = ($productPrice*$optionPrice)/100;
                    }

                    $totalPrice += $optionPrice;
                }
            }
        } else {
            foreach ($option->getValues() as $v) {
                if ($v->getOptionTypeId() == $value) {
                    $optionPrice = $v->getPrice();
                    $priceType = $v->getPriceType();
                    if ($priceType != "fixed") {
                        $optionPrice = ($productPrice*$optionPrice)/100;
                    }

                    $totalPrice += $optionPrice;
                }
            }
        }

        return $totalPrice;
    }

    public function getRemainingAmount($item)
    {
        $configurableOptions = ["drop_down", "radio", "checkbox", "multiple"];
        $multipleOptions = ["checkbox", "multiple"];
        $product = $item->getProduct();
        $options = $item->getBuyRequest()->getOptions();
        $originalPrice = $item->getOriginalPrice();
        $totalPrice = 0;
        try {
            foreach ($options as $key => $value) {
                $optionData = $product->getOptionById($key);
                if ($optionData) {
                    if (in_array($optionData->getType(), $configurableOptions)) {
                        if (in_array($optionData->getType(), $multipleOptions)) {
                            $totalPrice += $this->getOptionPrice($optionData, $value, $originalPrice, true);
                        } else {
                            $totalPrice += $this->getOptionPrice($optionData, $value, $originalPrice);
                        }
                    } else {
                        $optionPrice = $optionData->getPrice();
                        $priceType = $optionData->getPriceType();
                        if ($priceType != "fixed") {
                            $optionPrice = ($originalPrice*$optionPrice)/100;
                        }

                        $totalPrice += $optionPrice;
                    }
                }
            }
        } catch (\Exception $e) {
            $e->getMessage();
        }

        $remainingAmount = $originalPrice + $totalPrice;
        return $remainingAmount;
    }
}
