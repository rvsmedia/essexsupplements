<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Preorder
 * @author    Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\Preorder\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\RequestInterface;
use Webkul\Preorder\Model\ResourceModel\Item\CollectionFactory as Items;
use Webkul\Preorder\Model\ResourceModel\Preorder\CollectionFactory;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory as Products;

class CustomPrice implements ObserverInterface
{
    /**
     * @var RequestInterface
     */
    protected $_request;

    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $_customerSession;

    /**
     * @var \Magento\Framework\Message\ManagerInterface
     */
    protected $_messageManager;

    /**
     * @var \Webkul\Preorder\Model\ItemFactory
     */
    protected $_item;

    /**
     * @var \Webkul\Preorder\Helper\Data
     */
    protected $_preorderHelper;

    /**
     * @var Items
     */
    protected $_itemCollection;

    /**
     * @var CollectionFactory
     */
    protected $_preorderCollection;

    /**
     * @var Products
     */
    protected $_productCollection;

    /**
     * @param RequestInterface $request
     * @param \Magento\Customer\Model\Session $customerSession
     * @param \Magento\Framework\Message\ManagerInterface $messageManager
     * @param \Webkul\Preorder\Model\ItemFactory $item
     * @param \Webkul\Preorder\Helper\Data $preorderHelper
     * @param Items $itemCollection
     * @param CollectionFactory $preorderCollection
     * @param Products $productCollection
     */
    public function __construct(
        RequestInterface $request,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Webkul\Preorder\Model\ItemFactory $item,
        \Webkul\Preorder\Helper\Data $preorderHelper,
        Items $itemCollection,
        CollectionFactory $preorderCollection,
        Products $productCollection
    ) {
        $this->_request = $request;
        $this->_customerSession = $customerSession;
        $this->_messageManager = $messageManager;
        $this->_item = $item;
        $this->_preorderHelper = $preorderHelper;
        $this->_itemCollection = $itemCollection;
        $this->_preorderCollection = $preorderCollection;
        $this->_productCollection = $productCollection;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $helper = $this->_preorderHelper;
        $item = $observer->getEvent()->getData('quote_item');
        $product = $observer->getEvent()->getData('product');
        $productId = $this->getFinalProductId($product);
        $this->setPreorderPrice($item, $product, $productId);
        $preorderCompleteProductId = $helper->getPreorderCompleteProductId();
        if ($productId == $preorderCompleteProductId) {
            $result = $this->processPreorderCompleteData($item);
            if ($result['error']) {
                $this->_messageManager->addNotice(__($result['msg']));
                throw new \Magento\Framework\Exception\LocalizedException($result['msg']);
            }
        }
    }

    /**
     * Get Product Id
     *
     * @param object $product
     *
     * @return int
     */
    public function getFinalProductId($product)
    {
        $helper = $this->_preorderHelper;
        $productId = $product->getId();
        $data = $this->_request->getParams();
        if (array_key_exists('selected_configurable_option', $data)) {
            if ($data['selected_configurable_option'] != '') {
                $productId = $data['selected_configurable_option'];
            } else {
                if (array_key_exists('super_attribute', $data)) {
                    $info = $data['super_attribute'];
                    $productId = $helper->getAssociatedId($info, $product);
                }
            }
        }

        return $productId;
    }

    /**
     * Set Preorder Product Price
     *
     * @param object $item
     * @param object $product
     * @param int $productId
     *
     * @return int
     */
    public function setPreorderPrice($item, $product, $productId)
    {
        $helper = $this->_preorderHelper;
        $preorderPercent = $helper->getPreorderPercent();
        if ($helper->isPreorder($productId)) {
            $id = (int) $item->getId();
            $helper->setProductPrice($item);
            if ($id > 0) {
                $collection = $this->_itemCollection->create();
                $item = $helper->getDataByField($id, 'item_id', $collection);
                if ($item) {
                    $data = [
                                'item_id' => $id,
                                'preorder_percent' => $preorderPercent
                            ];
                    $this->_item->create()
                                ->addData($data)
                                ->setId($item->getId())
                                ->save();
                }
            }
        }
    }

    /**
     * Get Product Id
     *
     * @param object $quoteItem
     *
     * @return array
     */
    public function processPreorderCompleteData($quoteItem)
    {
        $itemId = (int) $quoteItem->getId();
        $helper = $this->_preorderHelper;
        $data = $this->_request->getParams();
        $qty = $data['qty'];
        $orderId = $data['order_id'];
        $orderItemId = $data['item_id'];
        $preorderProductId = $data['pro_id'];
        $stockStatus = 0;
        $preorderQty = 0;
        $collection = $this->_productCollection->create();
        $table = 'cataloginventory_stock_item';
        $bind = 'product_id = entity_id';
        $cond = '{{table}}.stock_id = 1';
        $type = 'left';
        $alias = 'is_in_stock';
        $field = 'is_in_stock';
        $collection->joinField($alias, $table, $field, $bind, $cond, $type);
        $alias = 'qty';
        $field = 'qty';
        $collection->joinField($alias, $table, $field, $bind, $cond, $type);
        $collection->addFieldToFilter('entity_id', $preorderProductId);
        foreach ($collection as $value) {
            $stockStatus = $value->getIsInStock();
            $preorderQty = $value->getQty();
        }

        if ($stockStatus == 0 || $qty > $preorderQty) {
            $msg = 'Product is not available.';
            $result = ['error'=> true, 'msg'=> $msg];
            return $result;
        }

        if ($itemId > 0) {
            $msg = 'Already added to cart.';
            $result = ['error'=> true, 'msg'=> $msg];
            return $result;
        }

        $collection = $this->_preorderCollection->create();
        $values = [$orderItemId, $orderId];
        $fields = ['item_id', 'order_id'];
        $item = $helper->getDataByField($values, $fields, $collection);
        if ($item) {
            $unitPrice = $helper->getUnitPrice($item);
            $quoteItem->setCustomPrice($unitPrice);
            $quoteItem->setOriginalCustomPrice($unitPrice);
            $quoteItem->getProduct()->setIsSuperMode(true);
            return ['error' => false];
        }

        $msg = 'Something went wrong.';
        $result = ['error'=> true, 'msg'=> $msg];
        return $result;
    }
}
