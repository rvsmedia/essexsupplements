<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Preorder
 * @author    Webkul
 * @copyright Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\Preorder\Observer;

use Magento\Framework\Event\ObserverInterface;
use Webkul\Preorder\Model\ResourceModel\Item\CollectionFactory as Items;
use Webkul\Preorder\Model\ResourceModel\Complete\CollectionFactory;
use Webkul\Preorder\Model\ResourceModel\Preorder\CollectionFactory as Preorder;

class AfterSaveShipment implements ObserverInterface
{
    /**
     * @var \Webkul\Preorder\Helper\Data
     */
    protected $_preorderHelper;

    /**
     * @var Preorder
     */
    protected $_preorderCollection;

    /**
     * @param \Webkul\Preorder\Helper\Data $preorderHelper
     * @param Preorder $preorderCollection
     */
    public function __construct(
        \Webkul\Preorder\Helper\Data $preorderHelper,
        Preorder $preorderCollection
    ) {
        $this->_preorderHelper = $preorderHelper;
        $this->_preorderCollection = $preorderCollection;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $data = $this->_preorderHelper->getParams();
        try {
            $itemIds = $data['shipment']['items'];
            $ids = [];
            foreach ($itemIds as $itemId => $qty) {
                $this->_preorderHelper->processShipmenmt($itemId, $qty);
            }
        } catch (\Exception $e) {
            $e->getMessage();
        }
    }
}
