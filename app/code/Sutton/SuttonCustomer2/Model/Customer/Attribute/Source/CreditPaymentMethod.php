<?php


namespace Sutton\SuttonCustomer2\Model\Customer\Attribute\Source;

class CreditPaymentMethod extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource
{
    /**
     * getAllOptions
     *
     * @return array
     */
    public function getAllOptions()
    {
        return [
            'paypal' => [
                'label' => 'Pay Pal',
                'value' => 'paypal'
            ],
            'creditelite' => [
                'label' => 'Credit Elite',
                'value' => 'creditelite'
            ],
            'creditfactoring' => [
                'label' => 'Credit Factoring',
                'value' => 'creditfactoring'
            ]
        ];
    }
}

